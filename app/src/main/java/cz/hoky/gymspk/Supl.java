package cz.hoky.gymspk;

public class Supl {

    private long id;
    private String trida;
    private String hodina;
    private String predmet;
    private String vyucujici;
    private String zastupujici;
    private String ucebna;
    private String typ;
    private String poznamka;
    private String duvod;

    private boolean posunuto;

    public Supl(String trida, String hodina, String predmet, String vyucujici, String zastupujici, String ucebna, String typ, String poznamka, String duvod) {
        this.trida = trida;
        this.hodina = hodina;
        this.predmet = predmet;
        this.vyucujici = vyucujici;
        this.zastupujici = zastupujici;
        this.ucebna = ucebna;
        this.typ = typ;
        this.poznamka = poznamka;
        this.duvod = duvod;
        this.posunuto = false;
    }

    public Supl(){

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTrida() {
        return trida;
    }

    public void setTrida(String trida) {
        this.trida = trida;
    }

    public String getHodina() {
        return hodina;
    }

    public void setHodina(String hodina) {
        this.hodina = hodina;
    }

    public String getPredmet() {
        return predmet;
    }

    public void setPredmet(String predmet) {
        this.predmet = predmet;
    }

    public String getVyucujici() {
        return vyucujici;
    }

    public void setVyucujici(String vyucujici) {
        this.vyucujici = vyucujici;
    }

    public String getZastupujici() {
        return zastupujici;
    }

    public void setZastupujici(String zastupujici) {
        this.zastupujici = zastupujici;
    }

    public String getUcebna() {
        return ucebna;
    }

    public void setUcebna(String ucebna) {
        this.ucebna = ucebna;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public String getPoznamka() {
        return poznamka;
    }

    public void setPoznamka(String poznamka) {
        this.poznamka = poznamka;
    }

    public String getDuvod() {
        return duvod;
    }

    public void setDuvod(String duvod) {
        this.duvod = duvod;
    }

    public boolean isPosunuto() {
        return posunuto;
    }

    public void setPosunuto(boolean posunuto) {
        this.posunuto = posunuto;
    }
}

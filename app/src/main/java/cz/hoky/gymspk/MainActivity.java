package cz.hoky.gymspk;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.common.collect.Multimap;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cz.hoky.gymspk.Hodina.HodinaComparator.ZKPREDMET_SORT;
import static cz.hoky.gymspk.Hodina.HodinaComparator.ZKUCITEL_SORT;
import static cz.hoky.gymspk.Hodina.HodinaComparator.getComparator;

public class MainActivity extends ActionBarActivity {

    int failedToDownloadCounter = 0;

    boolean downloadBtnClickable = true;

    public static Toast myToast;

    boolean firstRun = true;
    MyDBHandler dbHandler;

    DownloadSupl downloadSupl;

    RozvrhDAO rozvrhDAO;
    SuplDAO suplDAO;

    String gymplTableUrls = "http://www.gymspk.cz/rozvrh/hlavskol.htm";
    String gymplSuplyUrls = "http://www.gymspk.cz/suply/suplhlav.htm";
    String rootTableUrl = "http://www.gymspk.cz/rozvrh/";
    String rootSuplyUrl = "http://www.gymspk.cz/suply/";



    /*
    String gymplTableUrls = "http://www.oa-sumperk.cz/rozvrhy/rozvrh_tr_menu.htm";
    String gymplSuplyUrls = "http://www.oa-sumperk.cz/suplovani/zast.htm";
    String rootTableUrl = "http://www.oa-sumperk.cz/rozvrhy/";
    String rootSuplyUrl = "http://www.oa-sumperk.cz/suplovani";
    */

    List<Rozvrh> rozvrhy = new ArrayList<>();
    int tridaCounter = 0;
    List<String> casyList;
    String trida;
    CharSequence[] tridyChar;
    Timify tf = new Timify();
    Datify df = new Datify();
    Downloader down;

    LocalTime refreshTime;
    int newHour = 0;
    int autoFrequency;

    boolean zvyraznitMultiple;
    boolean zvyraznitNedefault;

    boolean allDefaultHodinySet = false;
    boolean userInformedDefaultHodiny = false;

    DrawTimeTable draw = new DrawTimeTable(this);

    List<String> tridy = new ArrayList<>();

    Map<LocalDate, Multimap<String, Supl>> suplMap = new HashMap<>();

    private Handler mHandler;

    private static final int SETTINGS_RESULT = 1;

    int notDefaultCounter = 1;
    int pocetZmen = 0;

    DownloadAndParse downloadAndParse = new DownloadAndParse();;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

        refreshTime = new LocalTime();

        //SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        userInformedDefaultHodiny = sharedPrefs.getBoolean("informed_default_hodiny", false);
        //autoFrequency = Integer.parseInt(sharedPrefs.getString("pref_key_supl_frequency", "11"));
        // zvyraznitMultiple = sharedPrefs.getBoolean("pref_key_zvyraznit_multiple", true);
        // zvyraznitNedefault = sharedPrefs.getBoolean("pref_key_zvyraznit_nedefault", true);


        myToast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
        dbHandler = MyDBHandler.getInstance(this);

        dbHandler.createTables();

        checkForSavedTrida();

        downloadSupl = new DownloadSupl();
        mHandler = new Handler();

        new AsyncLoadSuplyDB().execute();

        if (dbHandler.tableNotEmpty(MyDBHandler.TABLE_ROZVRHY)) {
            Log.d("DB", "Databaze i tabulka " + MyDBHandler.TABLE_ROZVRHY + " existuje");

            new AsyncLoadDB().execute((Void[]) null);

        } else {

            setContentView(R.layout.first_run_1);
            Log.d("DB", "Tabulka " + MyDBHandler.TABLE_ROZVRHY + " NEexistuje");

        }
    }


    Runnable drawRefresher = new Runnable() {
        @Override
        public void run() {
            draw.drawTableLayout(rozvrhy.get(tridaCounter), suplMap, MainActivity.this);
            mHandler.postDelayed(drawRefresher, 60000); // kazdou minutu refresh
            Log.d("Runnable", "redrawed!");
        }
    };

    public void onResume() {
        super.onResume();
        //setContentView(R.layout.activity_main);
        if (!firstRun) {
            if (rozvrhy.size() > 0) {
                if (rozvrhy.get(tridaCounter) != null) {
                    draw.drawTableLayout(rozvrhy.get(tridaCounter), suplMap, this);

                    /*if (autoSupl()){

                        if (refreshTime.getHourOfDay() - newHour > autoFrequency)
                            new DownloadSupl().execute();
                        newHour = refreshTime.getHourOfDay();
                    }*/

                } else {
                    draw.drawTableLayout(rozvrhy.get(0), suplMap, this);
                }
            } else {
                displayToast("Rozvrh není k dispozici");
            }
        }
        firstRun = false;
    }

    // Vrati pole se tridami
    public ArrayList<String> getTridyList() {
        ArrayList<String> tridyList = new ArrayList<>();
        for (Rozvrh rozvrh : rozvrhy) {
            tridyList.add(rozvrh.getTrida());
        }

        return tridyList;
    }

    // POPUp dialog se třídami - po vybraní vykreslí rozvrh dané třídy
    public void showSelectClass(final boolean intro) {
        AlertDialog dialog;
        String title;

        if (intro) {
            title = "Vyber svou výchozí třídu:";
        } else {
            title = "Vyber třídu:";
        }

        // Vytvori CharSequence z pole trid
        final ArrayList<String> seznamTrid = getTridyList();
        final CharSequence[] items = seznamTrid.toArray(new CharSequence[tridy.size()]);

        // Zobrazeni a populating pop-up dialogu
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        if (intro){
            builder.setCancelable(false);
        }
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int pos) {
                tridaCounter = pos;
                trida = seznamTrid.get(pos);
                if (intro) {
                    saveTrida();
                    helpDialog();
                }
                draw.drawTableLayout(rozvrhy.get(tridaCounter), suplMap, MainActivity.this);
            }
        });
        dialog = builder.create();
        dialog.show();
    }

    // Zkontroluje zda je v sharedPrefs ulozena hodnota s vychozi tridou - pokud ano prenastavi tridaCounter
    public String checkForSavedTrida() {

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        String savedTridaString = sharedPrefs.getString("vychozi_trida", "1.A");

        int i = 0;
        while (i < rozvrhy.size()) {
            if (rozvrhy.get(i).getTrida().equals(savedTridaString)) {
                tridaCounter = i;
                Log.d("Print: ", "trida counter found: " + savedTridaString + ", " + tridaCounter);
            }
            i++;
        }
        return savedTridaString;
    }

    public boolean autoSupl() {

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        Boolean auto = sharedPrefs.getBoolean("pref_key_supl_sync", false);

        return auto;
    }

    public void downloadRozvrhy(View view) {

        if (isNetworkConnected()) {
            if (downloadAndParse.getStatus() != AsyncTask.Status.RUNNING) {
                downloadAndParse = new DownloadAndParse();
                downloadAndParse.execute(gymplTableUrls, rootTableUrl);
            }
        } else {
            displayToast("Připojení k internetu není dostupné\nZkus to prosím znova");
        }
    }

    private class AsyncLoadDB extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd;

        protected void onPreExecute() {
            super.onPreExecute();

            pd = new ProgressDialog(MainActivity.this);
            pd.setMessage("Načítání rozvrhů");
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setCancelable(false);
            pd.show();
        }

        protected Void doInBackground(Void... param) {
            rozvrhDAO = new RozvrhDAO(getBaseContext());
            rozvrhy = rozvrhDAO.getAllRozvrhy();
            Log.d("Print: ", "DB loaded!");
            return null;
        }

        protected void onPostExecute(Void param) {
            if (pd.isShowing()) {
                pd.dismiss();
            }
            checkForSavedTrida();
            if (rozvrhy.size() > 0) {
                Log.d("Print: ", "drawing from DB!");
                draw.drawTableLayout(rozvrhy.get(tridaCounter), suplMap, MainActivity.this);
                drawRefresher.run();
            } else {
                displayToast("Nelze načíst suply z databáze\nProsím smaž databázi v Nastavení nebo přeinstaluj aplikaci");
            }
        }
    }

    private class AsyncLoadSuplyDB extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... param) {
            if (dbHandler.tableNotEmpty(MyDBHandler.TABLE_SUPLY)) {
                suplDAO = new SuplDAO(getBaseContext());
                suplMap = suplDAO.getAllSuplyAsMap();
            }
            return null;
        }
    }

    public class DownloadAndParse extends AsyncTask<String, Void, List<Rozvrh>> {

        ProgressDialog pd;
        long startTime;

        private boolean errorSeznam = false;

        protected void onPreExecute() {
            super.onPreExecute();

            startTime = System.currentTimeMillis();

            pd = new ProgressDialog(MainActivity.this);
            pd.setMessage("Stahování rozvrhů");
            pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pd.setProgress(0);
            pd.setCancelable(false);
            pd.show();
        }

        protected List<Rozvrh> doInBackground(String... urls) {
            down = new Downloader();
            try {
                ArrayList timetableUrls = extractUrlsFromSite(urls[0], urls[1]);
                pd.setMax(timetableUrls.size());
                for (int i = 0; i < timetableUrls.size(); i++) {
                    pd.setProgress(i);
                    Rozvrh rozvrh = down.downloadAndMakeTimetable(timetableUrls.get(i).toString(), MainActivity.this);
                    rozvrhy.add(rozvrh);
                }
            } catch (IOException e) {
                errorSeznam = true;
                rozvrhy.clear();
            }
            if (failedToDownloadCounter > 0) {
                rozvrhy.clear();
            }
            return rozvrhy;
        }

        @Override
        protected void onPostExecute(List<Rozvrh> rozvrhy) {
            Log.d("Print: ", "drawing!");
            if (pd.isShowing()) {
                pd.dismiss();
            }
            //checkForSavedTrida();

            if (failedToDownloadCounter > 0 || errorSeznam){
                displayToast("CHYBA!\nNepodařilo se stáhnout některé rozvrhy\nZkontroluj své připojení k internetu");

            } else {

                // ulozit do DB hromadne
                if (rozvrhy.size() > 0) {
                    showSelectClass(true);
                    rozvrhDAO = new RozvrhDAO(getBaseContext());
                    rozvrhDAO.saveAllRozvrhyToDb(rozvrhy);
                } else {
                    showErrorDialog("down_error_rozvrhy");
                }
            }

            /*
            if (rozvrhy.size() == 0){
                // chyba - nelze stahnout rozvrhy
                showErrorDialog("down_error_rozvrhy");
            } else if (rozvrhy.get(tridaCounter) == null){
                // chyba nelze nacist zvoleny rozvrh
            } else {
                draw.drawTableLayout(rozvrhy.get(tridaCounter), suplMap, MainActivity.this);
            }*/
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
//        automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            //noinspection SimplifiableIfStatement
            case R.id.about_app:

                Intent intentAbout = new Intent(this, AboutActivity.class);
                startActivity(intentAbout);
                return true;

            case R.id.napoveda:

                Intent intentHelp = new Intent(this, HelpActivity.class);
                startActivity(intentHelp);
                return true;

            case R.id.update_timetable:

                downloadNewSuply();

                return true;

            case R.id.show_supl_as_table:

                if (dbHandler.tableNotEmpty(MyDBHandler.TABLE_SUPLY)) {
                    Intent intentSupl = new Intent(this, ViewerSuplActivity.class);
                    startActivity(intentSupl);
                } else {
                    displayToast("Žádné suply k dispozici\nZkus stáhnout suply stisknutím tlačítka \"aktualizace\"");
                }
                return true;

            case R.id.menu_settings:

                Intent i = new Intent(this, SettingsActivity.class);
                ArrayList<String> tridyList = getTridyList();
                i.putExtra("tridyList", tridyList);
                startActivityForResult(i, SETTINGS_RESULT);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private class DownloadSupl extends AsyncTask<String, Void, Map<LocalDate, Multimap<String, Supl>>> {

        private boolean errorSeznam = false;

        SuplDAO suplDAO = new SuplDAO(MainActivity.this);
        int pocetSuplu = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        protected Map<LocalDate, Multimap<String, Supl>> doInBackground(String... urls) {
            down = new Downloader();


            //Multimap<String, Supl> suplMultimap = down.downloadAndMakeSupl("", MainActivity.this);
            if (suplMap == null) {
                suplMap = new HashMap<>();
            }

            try {

                ArrayList suplyUrls = extractUrlsFromSite(urls[0], urls[1]);
                pocetSuplu = suplyUrls.size();

                for (int i = 0; i < suplyUrls.size(); i++) {

                    // PRASARNA - extrakce data z url adresy...
                    String rawUrl = suplyUrls.get(i).toString();
                    String rawDate = rawUrl.substring(rawUrl.lastIndexOf("_") + 1, rawUrl.lastIndexOf("."));
                    LocalDate myDate = LocalDate.parse(rawDate, DateTimeFormat.forPattern("yyyyMMdd"));
                    Log.d("Url", rawDate);
                    Log.d("Url", myDate.toString("d.M.yyyy"));

                    if (suplDAO.suplWithDateExists(myDate)) {
                        pocetSuplu--;
                    } else {
                        Multimap<String, Supl> suplMultimap = down.downloadAndMakeSupl(suplyUrls.get(i).toString(), MainActivity.this);
                        suplMap.put(myDate, suplMultimap);
                    }

                }

            } catch (IOException e) {
                e.printStackTrace();
                errorSeznam = true;
            }
            //return null;
            Log.d("Pocet Novych Suplu", pocetSuplu + "");
            return suplMap;
        }

        protected void onPostExecute(Map<LocalDate, Multimap<String, Supl>> map) {
            Log.d("Print", "Suply loaded!");
            Log.d("SuplLoaded", map.toString());

            if (failedToDownloadCounter > 0 || errorSeznam){
                suplMap.clear();
                displayToast("CHYBA!\nNepodařilo se stáhnout některé suply\nZkontroluj své připojení k internetu");
            } else {

                suplMap = map;

                //stopAnimation();

                if (rozvrhy.size() > 0 && tridaCounter < rozvrhy.size()) {
                    draw.drawTableLayout(rozvrhy.get(tridaCounter), map, MainActivity.this);
                }

                String message;
                if (pocetSuplu > 0) {
                    suplDAO.saveAllSuplyToDb(map);

                    switch (pocetSuplu) {

                        case 1:
                            message = "Hurá, " + pocetSuplu + " nový supl úspěšně stažen!";
                            //message += "<BR>(celkem <font color = 'red'>" + pocetZmen + "</font> změna tvých předmětů)<BR>";
                            break;
                        case 2:
                            message = "Hurá, " + pocetSuplu + " nové suply úspěšně staženy!";
                            //message += "<BR>(celkem <font color = 'red'>" + pocetZmen + "</font> změny tvých předmětů)<BR>";
                            break;
                        case 3:
                            message = "Hurá, " + pocetSuplu + " nové suply úspěšně staženy!";
                            // message += "<BR>(celkem <font color = 'red'>" + pocetZmen + "</font> změny tvých předmětů)<BR>";
                            break;
                        case 4:
                            message = "Hurá, " + pocetSuplu + " nové suply úspěšně staženy!";
                            // message += "<BR>(celkem <font color = 'red'>" + pocetZmen + "</font> změny tvých předmětů)<BR>";
                            break;
                        default:
                            message = "Hurá, " + pocetSuplu + " nových suplů úspěšně staženo!";
                            // message += "<BR>(celkem <font color = 'red'>" + pocetZmen + "</font> změn tvých předmětů)<BR>";
                    }

                } else {
                    message = "Smůla, žádný nový supl";
                }


                if (pocetSuplu > 0) {

                    for (LocalDate datum : suplMap.keySet()) {
                        message += "<BR><B>" + datum.toString("d. M.") + "</B> - " + df.getDayAsString(datum.dayOfWeek().get() - 1);
                    }
                }

                // 1. Instantiate an AlertDialog.Builder with its constructor
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                //final CharSequence[] items = unsortedList.toArray(new CharSequence[unsortedList.size()]);

                builder.setTitle("Supl").setMessage(Html.fromHtml(message)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //
                    }
                });
            /*builder.setItems(items, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                }
            });*/

                builder.create().show();
            }

        }
    }

    public ArrayList<String> extractUrlsFromSite(String url, String rootUrl) throws IOException {

        ArrayList<String> urls = new ArrayList<>();
        Document page = Jsoup.connect(url).userAgent("Mozilla").get(); // pripojeni ke strance s rozvrhem, ulozeni do doc
        Log.d("JSwa", "Connected " + ", " + url);

        Elements options = page.select("option");

        for (Element option : options) {
            urls.add(rootUrl + option.attr("value"));
        }

        Log.d("urls: ", urls.toString());

        return urls;
    }

    public Hodina getDefaultHodina(List<Hodina> hodinyDne) {
        Hodina aktHod = new Hodina();
        if (hodinyDne.size() == 1) {
            aktHod = hodinyDne.get(0);
        } else if (hodinyDne.size() > 1) {
            int counter = 0;
            for (Hodina hodina : hodinyDne) {
                if (hodina.isDefault()) {
                    aktHod = hodinyDne.get(counter);
                    break;
                }
                counter++;
            }

            if (aktHod.getZkPredmet() == null) {
                return null;
            }
        }
        return aktHod;
    }

    public void showMultipleSubjects(final Collection<Hodina> hodiny) {
        AlertDialog dialog;
        String title = "Zvolit výchozí předmět:";

        myToast.cancel();

        final List<Hodina> hodinyList = new ArrayList<Hodina>(hodiny);
        //Collections.sort(hodinyList, Hodina.COMPARE_BY_ZKPREDMET);


        // Vytvori CharSequence z pole trid
/*
        Collections.sort(hodiny, new Comparator<Hodina>() {
            @Override
            public int compare(Hodina p1, Hodina p2) {

                String predmet1 = p1.getZkPredmet().toUpperCase();
                String predmet2 = p2.getZkPredmet().toUpperCase();

                return p1.; // Ascending
            }

        });*/

        Collections.sort(hodinyList, getComparator(ZKPREDMET_SORT, ZKUCITEL_SORT));
        int selectedSubjectId = checkDefaultSubject(hodinyList);

        List<String> unsortedList = new ArrayList<>();
        for (Hodina hodina : hodinyList) {
            unsortedList.add(hodina.getZkPredmet() + " | " + hodina.getZkVyucujici() + " | " + hodina.getZkUcebna());
        }

        /*
        final TreeSet<String> sortedList = new TreeSet<>(); // Setridene pole Stringu podle abecedy
        final List<String> unsortedList = new ArrayList<>(); // Setridene pole Stringu podle abecedy
        for (Hodina hodina: hodiny){
            unsortedList.add(hodina.getZkPredmet() + " | " + hodina.getZkVyucujici() + " | " + hodina.getZkUcebna());
        }*/
        final CharSequence[] items = unsortedList.toArray(new CharSequence[unsortedList.size()]);

        // Zobrazeni a populating pop-up dialogu
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setSingleChoiceItems(items, selectedSubjectId, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int pos) {
                Log.d("choosed", pos + " ");
                userChoosed(pos, hodinyList);
                dialog.dismiss();
            }
        });
        dialog = builder.create();
        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
/*
        if (requestCode == SETTINGS_RESULT) {
            displayUserSettings();
        }*/

    }

    private void displayUserSettings() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        String settings = "";
        settings = settings+"Trida: " + sharedPrefs.getString("vychozi_trida", "");
        settings = settings+"\nKontrolovat suply: "+ sharedPrefs.getBoolean("pref_key_supl_sync", false);
        settings = settings+"\nFrekvence kontrol: " + sharedPrefs.getString("pref_key_supl_frequency", "NOUPDATE");

        displayToast(settings, 1);
    }


    public void userChoosed(int choice, Collection<Hodina> hodiny) {
        int counter = 0;

        Hodina vychHodina = new Hodina();
        for (Hodina hodina : hodiny) {
            if (counter == choice) {
                hodina.setIsDefault(true);
                dbHandler.setDefault(hodina, true);
                vychHodina = hodina;
            } else {
                hodina.setIsDefault(false);
                dbHandler.setDefault(hodina, false);
            }
            counter++;

        }
        draw.drawTableLayout(rozvrhy.get(tridaCounter), suplMap, MainActivity.this);

        displayToast("Výchozí předmět nastaven na:\n" + vychHodina.getPredmet() + "\n" + vychHodina.getVyucujici() + "\n" + vychHodina.getUcebna());
    }

    public int checkDefaultSubject(Collection<Hodina> hodiny) {
        int pos = 0;
        for (Hodina hodina : hodiny) {
            if (hodina.isDefault()) {
                return pos;
            }
            pos++;
        }
        return -1;
    }

    public void saveTrida() {
        Log.d("Saving: ", trida);
        PreferenceManager.getDefaultSharedPreferences(getBaseContext()).edit().putString("vychozi_trida", trida).commit();
    }

    public void saveInfrormedDefaultHodiny() {
        PreferenceManager.getDefaultSharedPreferences(getBaseContext()).edit().putBoolean("informed_default_hodiny", true).commit();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null;
    }

    private Toast toast;
    private long lastBackPressTime = 0;

    @Override
    public void onBackPressed() {
        if (this.lastBackPressTime < System.currentTimeMillis() - 4000) {
            toast = Toast.makeText(this, "Stikni znovu zpět pro vypnutí aplikace", Toast.LENGTH_SHORT);
            toast.show();
            this.lastBackPressTime = System.currentTimeMillis();
        } else {
            if (toast != null) {
                toast.cancel();
            }
            super.onBackPressed();
        }
    }

    public void displayToast(final String message) {
        myToast.cancel();
        myToast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        myToast.show();
    }

    public void displayToast(final Spanned message) {
        myToast.cancel();
        myToast = Toast.makeText(this, message, Toast.LENGTH_LONG);
        myToast.show();
    }

    public void displayToast(final String message, int duration) {
        myToast.cancel();
        myToast = Toast.makeText(this, message, Toast.LENGTH_LONG);
        myToast.show();
    }

    public void showErrorDialog(final String error) {

        String message = "Chyba!";
        switch (error) {
            case "down_error_rozvrhy":
                message = "Nelze stáhnout rozvrhy\n1)Zkontroluj své připojení\n2)Stránka školy je nedostupná, zkus to znovu později";
        }

        AlertDialog dialog;
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Chyba").setMessage(message);
        builder.setNegativeButton("Zrušit",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
        builder.setPositiveButton("Zkusit znovu",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new DownloadAndParse().execute(gymplTableUrls, rootTableUrl);
                    }
                });
        dialog = builder.create();
        dialog.show();

    }

    public void helpDialog() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Vítej v aplikaci!");
        builder.setCancelable(false);
        builder.setMessage(getString(R.string.intro_disclamer_text));
        builder.setPositiveButton("Beru na vědomí", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int id){
            }
        });
        builder.show();
    }

    public void defaultHodinyWarningDialog() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Výchozí hodiny nenastaveny!");
        builder.setMessage(getString(R.string.vychozi_tridy_doporuceni_text));
        builder.setPositiveButton("I přesto stáhnout", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int id){
                downloadNewSuply();
            }
        });
        builder.setNegativeButton("Zrušit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        builder.show();
    }

    public void downloadNewSuply(){
        if (rozvrhy.size() > 0) {
            if (userInformedDefaultHodiny || notDefaultCounter == 0) {

                if (isNetworkConnected()) {
                    if (downloadSupl.getStatus() != AsyncTask.Status.RUNNING) {
                        displayToast("Kontroluji suply...");
                        //refreshList(item);
                        downloadSupl = new DownloadSupl();
                        downloadSupl.execute(gymplSuplyUrls, rootSuplyUrl);
                    } else {
                        displayToast("Stahování již v průběhu...");
                    }
                } else {
                    displayToast("Připojení k internetu není k dispozici");
                }

            } else {
                userInformedDefaultHodiny = true;
                defaultHodinyWarningDialog();
                saveInfrormedDefaultHodiny();
            }
        } else {
            displayToast("Nejdříve bude lepší stáhnout rozvrhy :)");
        }
    }


/*    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void refreshList(final MenuItem item) {
 Attach a rotating ImageView to the refresh item as an ActionView
        LayoutInflater inflater = (LayoutInflater) getApplication()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ImageView iv = (ImageView) inflater.inflate(R.layout.refresh_action_view,
                null);

        Animation rotation = AnimationUtils.loadAnimation(getApplication(),
                R.anim.refresh_rotate);
        rotation.setRepeatCount(Animation.INFINITE);
        iv.startAnimation(rotation);

        item.setActionView(iv);


        item.getActionView().clearAnimation();
        item.setActionView(null);

    }*/

}
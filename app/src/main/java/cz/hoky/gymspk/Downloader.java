package cz.hoky.gymspk;

import android.util.Log;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import org.joda.time.LocalDate;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Downloader {

    public Rozvrh downloadAndMakeTimetable(String url, MainActivity mainActivity) {

        mainActivity.failedToDownloadCounter = 0;

        Map<String, Multimap<String, Hodina>> rozvrh = new HashMap<>();
        List<String> dnyList = new ArrayList<>();
        List<String> casyList = new ArrayList<>();
        List<String> hodinyList = new ArrayList<>();
        String trida = "";
        int pocetHodin = 0;

        long time = System.currentTimeMillis();

        Log.d("JSwa", url);

        try {
            Document doc = Jsoup.connect(url).userAgent("Mozilla").get(); // pripojeni ke strance s rozvrhem, ulozeni do doc

            Log.d("JSwa", "Connected to [" + url + "]");

            trida = doc.select(".textlargebold_1").first().text().replace("\u00a0", ""); // trim &nbsp
            Element table = doc.select("table").get(1); // druha tabulka v html - ta nas zajima, obsahuje dany rozvrh

            /* ------ DEPRECATED  -----
            // Vytvori pole s hodinami (normalne 1-9, nektere skoly treba 0-10)
            Elements cislaHodin = table.select("tr.HlavickaHod").first().select("td");
            for (int i = 1; i < cislaHodin.size(); i++) {
                hodinyList.add(cislaHodin.get(i).text());
            }
            // Vytvori pole s casy rozvrhu (napr 8:00-8:45, ...)
            Elements casy = table.select("tr.HlavickaCas").first().select("td");
            for (Element cas : casy) {
                casyList.add(cas.text());
            }
            -------------> HARDCODED BELLOW */

            // Vytvori pole s hodinami (normalne 1-9, nektere skoly treba 0-10)
            for (int i = 1; i < 9; i++) { // cislaHodin.size();
                hodinyList.add(i+"");
            }
            // Vytvori pole s casy rozvrhu (napr 8:00-8:45, ...)
            String[] casyStrings = {"8:00- 8:45", "8:55- 9:40", "10:00-10:45", "10:55-11:40", "11:50-12:35", "12:45-13:30", "13:35-14:20", "14:25-15:10", "15:15-16:00"};
            Collections.addAll(casyList, casyStrings);


            // TODO: CODE BELLOW NEED FIXING
            Elements hodiny = table.select(".Hod");

            for (Element aktHodina : hodiny) {

                int aktHodInt; // aktualni hodina
                String doba = aktHodina.attr("title");
                String[] denHod = doba.split("\\*")[1].replaceAll("\\s", "").split(":"); // rozparsuje den a vuyc.hodinu/y aktualni hodiny

                Hodina hodina = new Hodina(); // vytvori objekt hodina, do ktere nasazi vsechny udaje
                hodina.setTrida(trida);
                hodina.setDen(denHod[0]);
                hodina.setHod(denHod[1]);

                // u title není třeba odstranovat &nbps - nezalomitelne mezery. u ostanich replace
                hodina.setPredmet((aktHodina.select("font.Predmet").first().attr("title")));
                hodina.setZkPredmet(aktHodina.select("font.Predmet").first().text().replaceAll("\u00A0", ""));
                hodina.setVyucujici((aktHodina.select("font.Vyucujuci").first().attr("title")));
                hodina.setZkVyucujici((aktHodina.select("font.Vyucujuci").first().text()).replaceAll("\u00A0", ""));
                hodina.setUcebna((aktHodina.select("font.Ucebna").first().attr("title")));
                hodina.setZkUcebna((aktHodina.select("font.Ucebna").first().text()).replaceAll("\u00A0", ""));
                hodina.setIsDefault(false);

                //Log.d("DownloadDBG", hodina.getPredmet()+" "+hodina.getPredmet().length()+ " "+hodina.getZkPredmet()+" "+hodina.getZkPredmet().length()+" "+hodina.getZkVyucujici()+hodina.getZkVyucujici().length()+" "+hodina.getZkUcebna()+" "+hodina.getZkUcebna().length());

                if (!dnyList.contains(hodina.den)) {
                    dnyList.add(hodina.den);
                }

                // Map of Multimap - naplni rozvrh - po, út... hodinama v dané hodině
                Multimap<String, Hodina> multimap = rozvrh.get(hodina.den);
                if (multimap == null) {
                    multimap = HashMultimap.create();
                    rozvrh.put(hodina.den, multimap);
                }

                // SPLITNE dvouhodiovku na 2 Hodiny
                if (hodina.hod.contains("-")) {

                    String[] dvouhod = hodina.hod.split("-");
                    aktHodInt = Integer.parseInt(dvouhod[0]) - 1; // ODECTE 1 - NUTNE UDELAT DYNAMICKY ZAJISTIT FUNKCNOST I PRO rozvrhy 0-10...
                    hodina.setHod(dvouhod[0]);
                    hodina.setIsDefault(false);
                    multimap.put(hodina.hod, hodina);
                    pocetHodin += 1;
                    //mainActivity.dbHandler.pridatHodinu(hodina); // +databaze

                    Hodina newHodina = new Hodina(hodina);
                    newHodina.setHod(dvouhod[1]);
                    newHodina.setIsDefault(false);
                    multimap.put(newHodina.hod, newHodina);
                    pocetHodin += 1;
                    //mainActivity.dbHandler.pridatHodinu(hodina); // + databaze

                } else {

                    aktHodInt = Integer.parseInt(hodina.hod) - 1; // ODECTE 1 - NUTNE UDELAT DYNAMICKY ZAJISTIT FUNKCNOST I PRO rozvrhy 0-10...
                    String[] casOdDo = casyList.get(aktHodInt).replaceAll("\\s", "").split("-");

                    multimap.put(hodina.hod, hodina);
                    pocetHodin += 1;
                    //mainActivity.dbHandler.pridatHodinu(hodina); //+ databaze
                }
            }

        } catch (Throwable t) {
            t.printStackTrace();
            mainActivity.failedToDownloadCounter ++;//mainActivity.displayToast("CHYBA!\nNepodařilo se stáhnout 1 nebo více rozvrhů!");
        }

        for (String den : dnyList) {
            for (String hodina : hodinyList) {
                if (!rozvrh.get(den).containsKey(hodina)) { // aktualni hodina je prazdna/volna/obed
                    int dalsiHod = Integer.parseInt(hodina) + 1;
                    if (rozvrh.get(den).containsKey(Integer.toString(dalsiHod))) {
                        // obed = true
                        Hodina obed = new Hodina();
                        obed.setPredmet("Oběd");
                        obed.setZkPredmet("");
                        obed.setVyucujici("");
                        obed.setZkVyucujici("");
                        obed.setZkUcebna("");
                        obed.setDen(den);
                        obed.setUcebna("Jídelna");
                        obed.setHod(hodina);
                        obed.setTrida(trida);
                        obed.setIsDefault(true);

                        int aktHodInt = Integer.parseInt(obed.hod) - 1; // ODECTE 1 - NUTNE UDELAT DYNAMICKY ZAJISTIT FUNKCNOST I PRO rozvrhy 0-10...
                        String[] casOdDo = casyList.get(aktHodInt).replaceAll("\\s", "").split("-");

                        Multimap<String, Hodina> tempMultimap = rozvrh.get(den);
                        if (tempMultimap == null) {
                            tempMultimap = HashMultimap.create();
                            rozvrh.put(den, tempMultimap);
                        }
                        tempMultimap.put(hodina, obed);
                        pocetHodin += 1;
                        //mainActivity.dbHandler.pridatHodinu(obed);

                    } else { // PRAZDNA
                        Hodina prazdna = new Hodina();
                        prazdna.setPredmet("Prázná");
                        prazdna.setZkPredmet("");
                        prazdna.setVyucujici("");
                        prazdna.setZkVyucujici("");
                        prazdna.setZkUcebna("");
                        prazdna.setUcebna("");
                        prazdna.setDen(den);
                        prazdna.setTrida(trida);
                        prazdna.setHod(hodina);
                        prazdna.setIsDefault(true);

                        int aktHodInt = Integer.parseInt(prazdna.hod) - 1; // ODECTE 1 - NUTNE UDELAT DYNAMICKY ZAJISTIT FUNKCNOST I PRO rozvrhy 0-10...
                        String[] casOdDo = casyList.get(aktHodInt).replaceAll("\\s", "").split("-");

                        Multimap<String, Hodina> tempMultimap = rozvrh.get(den);
                        if (tempMultimap == null) {
                            tempMultimap = HashMultimap.create();
                            rozvrh.put(den, tempMultimap);
                        }
                        tempMultimap.put(hodina, prazdna);
                        pocetHodin += 1;
                        //mainActivity.dbHandler.pridatHodinu(prazdna);
                    }
                }
            }
        }

        Log.d("Time Rozvrh", System.currentTimeMillis()- time + "ms");

        return new Rozvrh(trida, rozvrh, dnyList, hodinyList, mainActivity.tf.getCasyMap(), pocetHodin); // HARDCODED!
    }

    public Multimap<String, Supl> downloadAndMakeSupl(String url, MainActivity mainActivity){

        mainActivity.failedToDownloadCounter = 0;

        LocalDate datum = new LocalDate();
        Multimap<String, Supl> suplMultimap = HashMultimap.create();
        String strDatum = "";
        String TAG = "Supl";
        //String trida = "";
        //url = "http://www.gymspk.cz/suply/supl_20150205.htm";

        final String CELL_VYUCUJICI = "td.C11";
        final String CELL_DUVOD = "td.C12";
        final String CELL_TRIDA = "td.C13";
        final String CELL_HODINA = "td.C14";
        final String CELL_ZASTUPUJICI = "td.C15";
        final String CELL_PREDMET = "td.C16";
        final String CELL_UCEBNA = "td.C17";
        final String CELL_POZNAMKA = "td.C18";
        final String CELL_TYP = "td.C19";


        final String CELL_ZMENA_HODINA = "td.C22";
        final String CELL_ZMENA_TRIDA = "td.C23";
        final String CELL_ZMENA_PREDMET = "td.C24";
        final String CELL_ZMENA_ZASTUPUJICI = "td.C25";
        final String CELL_ZMENA_UCEBNA = "td.C26";
        final String CELL_ZMENA_POZNAMKA = "td.C27";

        //Pattern p = Pattern.compile("\\d+.\\d+.\\d+");

        try {
            long time = System.currentTimeMillis();

            Document doc = Jsoup.connect(url).userAgent("Mozilla").get(); // pripojeni ke strance s suplem, ulozeni do doc

            Log.d("JSwa", "Connected to [" + url + "]");
            //trida = doc.select("title").first().text();

            Elements radky;

            Element supl = doc.select(CELL_HODINA).first();
            if (supl != null) { // šestá tabulka v html - obsahuje změny učeben
                radky = supl.parent().parent().select("tr");

                // Projde radky a splitne radky ktere obsahuji vice trid na jednotlive radky
                for (Element radek : radky) {
                    if (radek.select(CELL_TRIDA).first() != null) {
                        //Log.d(TAG, radek.select(CELL_TRIDA).first().text());

                        if (radek.select(CELL_TRIDA).first().text().contains(",")) {
                            String[] tridy = radek.select(CELL_TRIDA).first().text().split("\\s*,\\s*");
                            for (String trida : tridy) {
                                Element novyRadek = radek.clone();
                                novyRadek.select(CELL_TRIDA).first().text(trida);
                                radek.before(novyRadek);
                            }
                            radek.remove();
                        }
                    }
                }

                // Projde radky znovu vytvori obj Supl z radku a nahazi do Multimapy
                supl = doc.select(CELL_HODINA).first();
                radky = supl.parent().parent().select("tr");
                for (Element radek : radky) {
                    if (radek.select(CELL_TRIDA).first() != null) {
                        Supl novySupl = new Supl();
                        novySupl.setTrida(radek.select(CELL_TRIDA).first().text().replaceAll("\u00A0", ""));
                        novySupl.setPredmet(radek.select(CELL_PREDMET).first().text().replaceAll("\u00A0", ""));
                        novySupl.setVyucujici(radek.select(CELL_VYUCUJICI).first().text().replaceAll("\u00A0", ""));

                        String zastupujici = radek.select(CELL_ZASTUPUJICI).first().text().replaceAll("\u00A0", "");
                        if (zastupujici.matches(".*[\\w]+.*")){
                            novySupl.setZastupujici(zastupujici);
                        } else {
                            novySupl.setZastupujici("");
                        }

                        novySupl.setHodina(radek.select(CELL_HODINA).first().text().replaceAll("\u00A0", ""));
                        novySupl.setDuvod(radek.select(CELL_DUVOD).first().text().replaceAll("\u00A0", ""));
                        novySupl.setUcebna(radek.select(CELL_UCEBNA).first().text().replaceAll("\u00A0", ""));
                        novySupl.setPoznamka(radek.select(CELL_POZNAMKA).first().text().replaceAll("\u00A0", ""));
                        novySupl.setTyp(radek.select(CELL_TYP).first().text().replaceAll("\u00A0", ""));
                        suplMultimap.put(novySupl.getTrida(), novySupl);
                    }
                }
            }

            // splitne multi tridy
            Element zmena = doc.select(CELL_ZMENA_HODINA).first();
            if (zmena != null) { // šestá tabulka v html - obsahuje změny učeben
                radky = zmena.parent().parent().select("tr");

                // Projde radky a splitne radky ktere obsahuji vice trid na jednotlive radky
                for (Element radek : radky) {
                    if (radek.select(CELL_ZMENA_TRIDA).first() != null) {
                        //Log.d(TAG, radek.select(CELL_TRIDA).first().text());

                        if (radek.select(CELL_ZMENA_TRIDA).first().text().contains(",")) {
                            String[] tridy = radek.select(CELL_ZMENA_TRIDA).first().text().split("\\s*,\\s*");
                            for (String trida : tridy) {
                                Element novyRadek = radek.clone();
                                novyRadek.select(CELL_ZMENA_TRIDA).first().text(trida);
                                radek.before(novyRadek);
                            }
                            radek.remove();
                        }
                    }
                }

                zmena = doc.select(CELL_ZMENA_HODINA).first();
                radky = zmena.parent().parent().select("tr");
                for (Element radek : radky) {
                    if (radek.select(CELL_ZMENA_TRIDA).first() != null) {
                        Supl novaZmena = new Supl();
                        novaZmena.setTrida(radek.select(CELL_ZMENA_TRIDA).first().text().replaceAll("\u00A0", ""));
                        novaZmena.setPredmet(radek.select(CELL_ZMENA_PREDMET).first().text().replaceAll("\u00A0", ""));
                        novaZmena.setVyucujici(radek.select(CELL_ZMENA_ZASTUPUJICI).first().text().replaceAll("\u00A0", ""));
                        novaZmena.setZastupujici("");
                        novaZmena.setDuvod("");
                        novaZmena.setTyp("Změna učebny");
                        novaZmena.setHodina(radek.select(CELL_ZMENA_HODINA).first().text().replaceAll("\u00A0", ""));
                        novaZmena.setUcebna(radek.select(CELL_ZMENA_UCEBNA).first().text().replaceAll("\u00A0", ""));
                        novaZmena.setPoznamka(radek.select(CELL_ZMENA_POZNAMKA).first().text().replaceAll("\u00A0", ""));
                        suplMultimap.put(novaZmena.getTrida(), novaZmena);
                    }
                }
            }

            Element poznamka = doc.select("tr.Oznam").first();
            if (poznamka != null){
                String poznamkaHtml = poznamka.select("td").first().html();
                Supl poznamkaSupl = new Supl("","","","","","","POZN",poznamkaHtml,"");
                suplMultimap.put("POZN", poznamkaSupl);
            }

            Log.d(TAG, System.currentTimeMillis()-time+"ms");
        }
        catch (Throwable t) {
            t.printStackTrace();
            mainActivity.failedToDownloadCounter ++;
        }
        return suplMultimap;
    }

}

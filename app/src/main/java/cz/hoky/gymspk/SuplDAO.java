package cz.hoky.gymspk;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

public class SuplDAO {

    public static final String TAG = "SuplDAO";

    private Context mContext;

    private SQLiteDatabase db;
    private MyDBHandler myDBHandler;
    private String[] mAllColumns = {MyDBHandler.COLUMN_SUPL_TRIDA, MyDBHandler.COLUMN_SUPL_HODINA, MyDBHandler.COLUMN_SUPL_PREDMET,
            MyDBHandler.COLUMN_SUPL_VYUCUJICI, MyDBHandler.COLUMN_SUPL_ZASTUPUJICI, MyDBHandler.COLUMN_SUPL_UCEBNA,
            MyDBHandler.COLUMN_SUPL_TYP, MyDBHandler.COLUMN_SUPL_POZNAMKA, MyDBHandler.COLUMN_SUPL_DUVOD};

    public SuplDAO(Context context) {
        myDBHandler = MyDBHandler.getInstance(context);
        this.mContext = context;

        try{
            open();
        } catch (SQLException e){
            Log.e(TAG, "SQLException on openning DB "+ e.getMessage());
        }
    }

    public void open() throws SQLException {
        db = myDBHandler.getWritableDatabase();
    }

    public void close(){
        myDBHandler.close();
    }

    public boolean suplWithDateExists(LocalDate myDate){
        String date = myDate.toString("dd.MM.yyyy");
        Cursor c = db.rawQuery("SELECT 1 FROM "+ MyDBHandler.TABLE_SUPLY +" WHERE "+ MyDBHandler.COLUMN_SUPL_DEN +" = ? ", new String[] {date});
        boolean exists = c.moveToFirst();
        c.close();
        return exists;
    }

    public void saveAllSuplyToDb(Map<LocalDate, Multimap<String, Supl>> suplMap) {

        int pocetNovychSuplu = 0;
        int pocetJizUlozenychSuplu = 0;
        long time = System.currentTimeMillis();

        String sql = "INSERT INTO " + MyDBHandler.TABLE_SUPLY + " ("
                + MyDBHandler.COLUMN_SUPL_DEN + ", "
                + MyDBHandler.COLUMN_SUPL_TRIDA + ", "
                + MyDBHandler.COLUMN_SUPL_HODINA + ", "
                + MyDBHandler.COLUMN_SUPL_PREDMET + ", "
                + MyDBHandler.COLUMN_SUPL_VYUCUJICI + ", "
                + MyDBHandler.COLUMN_SUPL_ZASTUPUJICI + ", "
                + MyDBHandler.COLUMN_SUPL_UCEBNA + ", "
                + MyDBHandler.COLUMN_SUPL_TYP + ", "
                + MyDBHandler.COLUMN_SUPL_POZNAMKA + ", "
                + MyDBHandler.COLUMN_SUPL_DUVOD + ") VALUES (?,?,?,?,?,?,?,?,?,?);";

        SQLiteStatement stmt = db.compileStatement(sql);
        db.beginTransaction();
        try {
            for (Map.Entry<LocalDate, Multimap<String, Supl>> entry: suplMap.entrySet()){
                LocalDate date = entry.getKey();
                String datum = date.toString("dd.MM.yyyy");

                if(!isDateSaved(datum)) {

                    pocetNovychSuplu ++;
                    Multimap<String, Supl> map = entry.getValue();
                    for (Supl supl : map.values()) {
                        stmt.clearBindings();
                        stmt.bindString(1, datum);
                        stmt.bindString(2, supl.getTrida());
                        stmt.bindString(3, supl.getHodina());
                        stmt.bindString(4, supl.getPredmet());
                        stmt.bindString(5, supl.getVyucujici());
                        stmt.bindString(6, supl.getZastupujici());
                        stmt.bindString(7, supl.getUcebna());
                        stmt.bindString(8, supl.getTyp());
                        stmt.bindString(9, supl.getPoznamka());
                        stmt.bindString(10, supl.getDuvod());
                        stmt.execute();
                    }
                } else {
                    pocetJizUlozenychSuplu ++;
                }
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            //fucked up - nepovedlo se rozvrhy ulozit do databaze ( ukazat toast nebo dialog)
            Log.d("Shit!", "No data saved");
            e.printStackTrace();

        } finally{
            db.endTransaction();
        }

        Log.d("Transaction complete", System.currentTimeMillis()-time+"ms, pocetNovych: "+pocetNovychSuplu +", pocetJizUlozenych: "+pocetJizUlozenychSuplu);
    }

    private boolean isDateSaved(String datum) {
        //db = myDBHandler.getReadableDatabase();
        String Query = "SELECT * FROM " + MyDBHandler.TABLE_SUPLY + " WHERE " + MyDBHandler.COLUMN_DEN + " = '" + datum + "'";
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public Supl createSupl(String trida, String hodina, String predmet, String vyucujici, String zastupujici, String ucebna, String typ, String poznamka, String duvod){
        ContentValues values = new ContentValues();
        values.put(MyDBHandler.COLUMN_SUPL_TRIDA, trida);
        values.put(MyDBHandler.COLUMN_SUPL_HODINA, hodina);
        values.put(MyDBHandler.COLUMN_SUPL_PREDMET, predmet);
        values.put(MyDBHandler.COLUMN_SUPL_VYUCUJICI, vyucujici);
        values.put(MyDBHandler.COLUMN_SUPL_ZASTUPUJICI, zastupujici);
        values.put(MyDBHandler.COLUMN_SUPL_UCEBNA, ucebna);
        values.put(MyDBHandler.COLUMN_SUPL_TYP, typ);
        values.put(MyDBHandler.COLUMN_SUPL_POZNAMKA, poznamka);
        values.put(MyDBHandler.COLUMN_SUPL_DUVOD, duvod);
        long insertId = db.insert(MyDBHandler.TABLE_SUPLY, null, values);
        Cursor cursor = db.query(MyDBHandler.TABLE_SUPLY, mAllColumns, MyDBHandler.COLUMN_SUPL_ID + " = " + insertId, null, null, null, null);
        cursor.moveToFirst();
        Supl novySupl = cursorToSupl(cursor);
        cursor.close();
        return novySupl;
    }

    public void deleteSupl(Supl supl){
        long id = supl.getId();
        Log.d(TAG, "Deleted supl with id: " + id);
        db.delete(MyDBHandler.TABLE_SUPLY, MyDBHandler.COLUMN_SUPL_ID + " = " + id, null);
    }

    public Map<LocalDate, Multimap <String, Supl>> getAllSuplyAsMap(){
        Multimap<String, Supl> suplMultimap = HashMultimap.create();
        Map<LocalDate, Multimap <String, Supl>> suplMap = new HashMap<>();

        String query = "SELECT * FROM " + MyDBHandler.TABLE_SUPLY + " WHERE 1 ORDER BY " + MyDBHandler.COLUMN_SUPL_DEN + " ASC";
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        String dt = cursor.getString(cursor.getColumnIndex(MyDBHandler.COLUMN_SUPL_DEN));
        String datum = "";

        while (!cursor.isAfterLast()) {

            Supl supl = cursorToSupl(cursor);
            datum = cursor.getString(cursor.getColumnIndex(MyDBHandler.COLUMN_SUPL_DEN));
            suplMultimap.put(supl.getTrida(), supl);

            if (!datum.equals(dt)){
                suplMap.put(new LocalDate(LocalDate.parse(dt, DateTimeFormat.forPattern("dd.MM.yyyy"))), suplMultimap);
                suplMultimap = HashMultimap.create();
                dt = datum;
            }
            cursor.moveToNext();

        }
        suplMap.put(new LocalDate(LocalDate.parse(datum, DateTimeFormat.forPattern("dd.MM.yyyy"))), suplMultimap);

        cursor.close();
        return suplMap;
    }

    public List<Supl> getSuplyByDateAsArray(LocalDate date){

        List<Supl> suplList = new ArrayList<>();
        String stringDate = date.toString("dd.MM.yyyy");

        String query = "SELECT * FROM " + MyDBHandler.TABLE_SUPLY + " WHERE " + MyDBHandler.COLUMN_DEN + " = '" + stringDate + "'  ORDER BY " + MyDBHandler.COLUMN_TRIDA + ", " + MyDBHandler.COLUMN_HODINA;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {

            Supl supl = cursorToSupl(cursor);
            suplList.add(supl);
            cursor.moveToNext();

        }

        cursor.close();
        return suplList;
    }

    public TreeSet<LocalDate> getDates(){
        TreeSet<LocalDate> dates = new TreeSet<>();
        String query = "SELECT * FROM " + MyDBHandler.TABLE_SUPLY + " WHERE 1 ORDER BY " + MyDBHandler.COLUMN_SUPL_DEN + " ASC";
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        String aktDate = cursor.getString(1);
        dates.add(new LocalDate(LocalDate.parse(aktDate, DateTimeFormat.forPattern("dd.MM.yyyy"))));

        while (!cursor.isAfterLast()) {

            if (!cursor.getString(1).equals(aktDate)){
                aktDate = cursor.getString(1);
                dates.add(new LocalDate(LocalDate.parse(aktDate, DateTimeFormat.forPattern("dd.MM.yyyy"))));
            }
            cursor.moveToNext();
        }

        cursor.close();
        return dates;
    }

    private Supl cursorToSupl(Cursor cursor){
        Supl supl = new Supl();
        supl.setId(cursor.getLong(0));
        supl.setTrida(cursor.getString(2));
        supl.setHodina(cursor.getString(3));
        supl.setPredmet(cursor.getString(4));
        supl.setVyucujici(cursor.getString(5));
        supl.setZastupujici(cursor.getString(6));
        supl.setUcebna(cursor.getString(7));
        supl.setTyp(cursor.getString(8));
        supl.setPoznamka(cursor.getString(9));
        supl.setDuvod(cursor.getString(10));
        return supl;
    }

}

package cz.hoky.gymspk;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.collect.BiMap;
import com.google.common.collect.Multimap;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class DrawTimeTable {

    public static Toast myToast;

    DecimalFormat decimalFormat=new DecimalFormat("#");
    Timify tf = new Timify();
    Ucitele ucitele = new Ucitele();
    BiMap zkratky = ucitele.getZkratky();

    private Context mContext;

    int quizCount = 0;
    int prevQuizCount = 0;
    int quizValue = 0;

    public DrawTimeTable(Context context) {
        mContext = context;
    }

    public void drawTableLayout(final Rozvrh rozvrhObj, Map<LocalDate, Multimap<String, Supl>> suplMap, final MainActivity mainActivity) {


        myToast = Toast.makeText(mContext, "", Toast.LENGTH_SHORT);

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(mainActivity.getBaseContext());
        String savedTridaString = sharedPrefs.getString("vychozi_trida", "1.A");
        Log.d("Saved Trida: ", savedTridaString);

        mainActivity.notDefaultCounter = 0;
        mainActivity.pocetZmen = 0;

        int zmenaCounter = 0;
        int suplCounter = 0;

        long cas = System.currentTimeMillis();

        String currentHodina = mainActivity.tf.getCurrentHodina(rozvrhObj.getHodiny(), rozvrhObj.getCasyMap());
        if (currentHodina != null) {
            currentHodina = getAktualniHodInclBreak(currentHodina);
            Log.d("currentHodina", currentHodina + "");
        }

        List<LocalDate> currentWeekDates = mainActivity.df.getDatesOfWeek(0);

        int dayOfWeek = new LocalDate().getDayOfWeek();

        List<String> dnyList = rozvrhObj.getDny();
        List<String> hodinyList = rozvrhObj.getHodiny();
        Map<String, Multimap<String, Hodina>> rozvrh = rozvrhObj.getRozvrh();
        mainActivity.trida = rozvrhObj.getTrida();
        //mainActivity.saveTrida();

        TableLayout table = new TableLayout(mainActivity);
        table.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT));

        TableRow.LayoutParams textviewParams = new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT, 1f);
        int dpValue = 1;
        float d = mainActivity.getResources().getDisplayMetrics().density;
        int margin = (int) (dpValue * d); // margin in pixels
        textviewParams.setMargins(margin, margin, margin, margin);

        int id = 1;

        Log.d("debug: ", "dnySize: " + dnyList.size() + " hodinySize: " + hodinyList.size());

        boolean suplThisDayExists = false;
        LocalDate aktualniDatum;

        for (int i = 0; i < hodinyList.size() + 1; i++) { //10x - radek (1.)-infoRadek=dny

            TableRow tableRow = new TableRow(mainActivity);
            tableRow.setOrientation(TableLayout.HORIZONTAL);

            String aktualniHodina;
            final String realAktualniHodina;

            if (i != 0) {
                realAktualniHodina = hodinyList.get(i - 1);

            } else {
                realAktualniHodina = hodinyList.get(i);
            }

            for (int j = 0; j < dnyList.size() + 1; j++) { // 6x sloupec (1.)-infoSloupek=hodiny+casy


                final String aktualniDen;
                if (j != 0) {
                    aktualniDen = dnyList.get(j - 1);
                } else {
                    aktualniDen = dnyList.get(j);

                }

                if (rozvrh.get(aktualniDen).containsKey(realAktualniHodina)) {
                    aktualniHodina = realAktualniHodina;
                } else {
                    aktualniHodina = realAktualniHodina.split(":")[0];
                }

                TextView textView = new TextView(mainActivity);
                textView.setHorizontallyScrolling(true);
                textView.setGravity(Gravity.CENTER);
                textView.setId(id++);
                textView.setTag(textView.getId());


                if (j == 0 && i == 0) { //Prvni pole = tlatiko SelectRozvrh
                    textView.setText(mainActivity.trida);
                    textView.setTextColor(Color.BLACK);
                    textView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Log.d("Print: ", mainActivity.tridaCounter + "");
                            mainActivity.showSelectClass(false);
                        }
                    });
                    textView.setBackgroundColor(Color.WHITE);
                } else if (j == 0) { //prvni sloupec - cisla a casy hodin

                    List<List<String>> casy = mainActivity.tf.getCasyList();
                    textView.setText(Html.fromHtml("<B>" + realAktualniHodina + "</B><BR><font color=\"#1A1A1A\"><small>" + casy.get(i - 1).get(0) + "<BR>" + casy.get(i - 1).get(1) + "</small></font>"));

                    textView.setTextColor(Color.BLACK);
                    textView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getQuizValue(Integer.parseInt(realAktualniHodina));
                        }
                    });
                    if (realAktualniHodina.equals(currentHodina)) {
                        textView.setBackgroundColor(0xff3399ff); // modra
                    } else {
                        textView.setBackgroundColor(0xff777777); // Tmave seda
                    }
                } else if (i == 0) { // prvni radek nazvy dni


                    // Log.d("DrawSupl", suplMap.toString());
                    // Log.d("DrawSupl", currentWeekDates.get(j-1).toString() +" "+suplThisDayExists);

                    textView.setText(Html.fromHtml("<small>" + currentWeekDates.get(j - 1).toString("d.M") + "</small><BR>" + aktualniDen));

                    textView.setTextColor(Color.BLACK);
                    textView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    if (j == dayOfWeek) {
                        textView.setBackgroundColor(0xff3399ff); // modra
                    } else {
                        textView.setBackgroundColor(0xff777777); // Tmave seda
                    }

                } else { // Predmety
                    List<Hodina> hodinyDne = new ArrayList<>(rozvrh.get(aktualniDen).get(aktualniHodina));
                    Hodina tempAktHod = mainActivity.getDefaultHodina(hodinyDne);
                    boolean zadnaDefault = false;
                    if (tempAktHod == null) {
                        tempAktHod = hodinyDne.get(0);
                        zadnaDefault = true;
                        mainActivity.notDefaultCounter += 1;
                    }

                    final Hodina aktHod = tempAktHod;


                    int pocetHodin = rozvrhObj.getRozvrh().get(aktualniDen).get(aktHod.getHod()).size();

                    if (aktHod.vyucujici != null) {
                        textView.setText(Html.fromHtml("<B>" + aktHod.getZkPredmet() + "</B><BR>" + aktHod.getZkVyucujici() + " " + aktHod.getZkUcebna()));
                    } else {
                        textView.setText(aktHod.zkPredmet);
                    }
                    if (zadnaDefault/* && mainActivity.zvyraznitNedefault*/) {
                        textView.setTextColor(Color.parseColor("#555555"));
                    } else {
                        textView.setTextColor(Color.BLACK);
                    }

                    if (pocetHodin <= 1/* && mainActivity.zvyraznitMultiple*/) {
                        textView.setBackgroundColor(Color.LTGRAY);
                    } else {
                        textView.setBackgroundColor(0xffbbbbbb); // Tmavsi seda
                    }

                    // zvyrazni aktualni predmet modre
                    if (realAktualniHodina.equals(currentHodina) && j == dayOfWeek) {
                        textView.setBackgroundColor(0xff3399ff); // modra
                    }

                    if (pocetHodin > 1) {
                        textView.setOnLongClickListener(new View.OnLongClickListener() {
                            @Override
                            public boolean onLongClick(View v) {
                                Collection<Hodina> hodiny = rozvrhObj.getRozvrh().get(aktualniDen).get(aktHod.hod);
                                mainActivity.showMultipleSubjects(hodiny);
                                return false;
                            }
                        });

                    }

                    textView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!aktHod.getZkVyucujici().equals("")) {
                                // open Dialog
                                int pocetHodin = rozvrhObj.getRozvrh().get(aktualniDen).get(aktHod.hod).size();
/*
                                SpannableString spannableString = new SpannableString("This is red text");
                                spannableString.setSpan(
                                        new ForegroundColorSpan(MainActivity.getResources().getColor(android.R.color.holo_red_light)),
                                        0,
                                        spannableString.length(),
                                        0);
                                Toast.makeText(this, spannableString, Toast.LENGTH_SHORT).show();*/


                                mainActivity.displayToast(
                                        aktHod.getPredmet() + "\n"
                                                + aktHod.getVyucujici() + "\n"
                                                + aktHod.getUcebna());

                            } else if (aktHod.predmet.equals("Oběd")) {
                                mainActivity.displayToast("Oběd");
                            } else {
                                mainActivity.displayToast("Domůůůůů!");
                            }
                        }
                    });

                    // ROUND CORNERS
                    /*
                    GradientDrawable shape =  new GradientDrawable() textView.getBackground();
                    shape.setCornerRadius( 12 );

                    int sdk = android.os.Build.VERSION.SDK_INT;
                    if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        textView.setBackgroundDrawable(shape);
                    } else {
                        textView.setBackground(shape);
                    }*/


                    if (suplMap != null) {

                        aktualniDatum = currentWeekDates.get(j - 1);
                        if (suplMap.containsKey(aktualniDatum)) { // Existuje supl v tomto dni
                            if (suplMap.get(aktualniDatum).containsKey(mainActivity.trida)) { // V tomto dni ma supl tato trida

                                List<Supl> hoursToAdd = new ArrayList<>();



                                for (final Supl supl : suplMap.get(aktualniDatum).get(mainActivity.trida)) {



                                    boolean zmenaUcebny = false;
                                    boolean zastup = false;
                                    boolean odpada = false;
                                    boolean posun = false;
                                    boolean zmenaPredmetu = false;

                                    if (aktualniHodina.equals(supl.getHodina())) { // AKTUALNI HODINA OBSAHUJE SUPL
                                        if (aktHod.getZkVyucujici().equals(supl.getVyucujici()) || (supl.getVyucujici().equals("") && pocetHodin < 2) || supl.getTyp().equals("Posunuto") || supl.getTyp().equals("Posun") ) { /*|| aktHod.getZkPredmet().equals(supl.getPredmet()) || aktHod.getZkUcebna().equals(supl.getUcebna())*/ // JE SUPL

                                            mainActivity.pocetZmen += 1;

                                            String ucebnaHtml = "";
                                            String predmetHtml;
                                            String vyucujiciHtml;

                                            if (supl.getTyp().equals("Posun") && !supl.isPosunuto()) {
                                                supl.setPosunuto(true);

                                                String posunZKtere = supl.getPoznamka().replaceAll("\\D+", "");
                                                List<Supl> actualSuplList = new ArrayList<>(suplMap.get(aktualniDatum).get(mainActivity.trida));
                                                int counter = actualSuplList.size() - 1;
                                                for (Supl supl1 : actualSuplList) {
                                                    if (supl1.getHodina().equals(posunZKtere)) { // dana hodina neni suplovana - vytvori ze odpadla
                                                        break;
                                                    }
                                                    if (counter == 0) {
                                                        Supl newPosun = new Supl(mainActivity.trida, posunZKtere, supl.getPredmet(), "", "", "", "Posunuto", "Posunuto na " + supl.getHodina() + ". hodinu", "");
                                                        hoursToAdd.add(newPosun);
                                                        Log.d("Posunuto", "vytvoreno - " + newPosun.getPredmet() + " " + newPosun.getHodina());
                                                    }
                                                    counter--;
                                                }

                                            }

                                            if (aktHod.getZkUcebna().equals(supl.getUcebna())) {
                                                ucebnaHtml = aktHod.getZkUcebna();
                                            } else if (supl.getUcebna().equals("") && supl.getZastupujici().equals("")) {
                                                odpada = true;
                                            } else {
                                                ucebnaHtml = "<font color='red'>" + supl.getUcebna() + "</font>";
                                                zmenaUcebny = true;
                                            }
                                            if (aktHod.getZkPredmet().equals(supl.getPredmet())) {
                                                predmetHtml = aktHod.getZkPredmet();
                                            } else {
                                                predmetHtml = "<font color='red'>" + supl.getPredmet() + "</font>";
                                                zmenaPredmetu = true;
                                            }

                                            // POROVNAT zkVYUCUJICI se ZASTUPUJICI (cele jmeno) - nutna databaze zkratek vyucujicich
                                            // pokud existuje zkratka vrati zkratku ze slovniku, jinak 2 prvni pismena z prijmeni
                                            String zkratka = "";
                                            if (supl.getZastupujici().equals("") && !supl.getUcebna().equals(aktHod.getZkUcebna())) {
                                                zmenaUcebny = true;
                                                zkratka = aktHod.getZkVyucujici();

                                            } else {
                                                Object value = zkratky.get(supl.getZastupujici());
                                                if (value != null) {
                                                    zkratka = value.toString();
                                                } else {
                                                    if (supl.getZastupujici().length() > 2) {
                                                        zkratka = supl.getZastupujici().substring(0, 2);
                                                    }
                                                }
                                            }

                                            if (aktHod.getZkVyucujici().equals(zkratka)) {
                                                vyucujiciHtml = aktHod.getZkVyucujici(); // Vyucujici je stejny
                                            } else {
                                                vyucujiciHtml = "<font color='red'>" + zkratka + "</font>"; // Zastupujici cervene
                                                zastup = true;
                                            }

                                            if (odpada) {
                                                textView.setText(Html.fromHtml("<font color='red'><B>" + aktHod.getZkPredmet() + "</B></font>"));
                                                textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                                            } else {
                                                textView.setText(Html.fromHtml("<B>" + predmetHtml + "</B><BR>" + vyucujiciHtml + " " + ucebnaHtml));
                                            }

                                            if ((supl.getPredmet().equals("") || supl.getVyucujici().equals("")) && !supl.getTyp().equals("Posunuto")) {
                                                if (supl.getPredmet().equals("")) {
                                                    textView.setText(Html.fromHtml("<B><font color='red'>?</B><BR>" + vyucujiciHtml + " " + ucebnaHtml));
                                                } else {
                                                    textView.setText(Html.fromHtml("<B>" + predmetHtml + "</B><BR>" + vyucujiciHtml + " " + ucebnaHtml));
                                                }
                                            }

                                            Log.d("Supl", aktualniDatum.toString("d.M.") + " (" + aktHod.getHod() + "): "
                                                    + aktHod.zkPredmet + " " + aktHod.zkVyucujici + " " + aktHod.zkUcebna + " -> "
                                                    + supl.getPredmet() + " " + ((supl.getZastupujici() != null) ? supl.getZastupujici() + " " : "") + supl.getUcebna());

                                            final boolean finalOdpada = odpada;
                                            final boolean finalZmenaPredmetu = zmenaPredmetu;
                                            final boolean finalZastup = zastup;
                                            final boolean finalZmenaUcebny = zmenaUcebny;
                                            textView.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    //if (!aktHod.getZkVyucujici().equals("")) {

                                                    String aktHodInfo = "";

                                                    if (!aktHod.getPredmet().equals("")){
                                                        aktHodInfo += aktHod.getPredmet() + "<BR>";
                                                    }
                                                    if (!aktHod.getVyucujici().equals("")){
                                                        aktHodInfo += aktHod.getVyucujici() + "<BR>";
                                                    }
                                                    if (!aktHod.getUcebna().equals("")){
                                                        aktHodInfo += aktHod.getUcebna() + "<BR>";
                                                    }

                                                    String text;

                                                    if (finalOdpada) {

                                                        if (!supl.getPoznamka().equals("")) {
                                                            String[] odpad = supl.getPoznamka().split(" ", 2);
                                                            odpad[0] = odpad[0].replace(",", "");
                                                            text = aktHodInfo + "<BR><font color='red'>" + odpad[0] + "</font>";
                                                            if (odpad.length > 1) {
                                                                text += "<BR><font color='red'>" + odpad[1] + "</font>";
                                                            }
                                                        } else {
                                                            text = aktHodInfo + "<BR><font color='red'> ? </font>";
                                                        }
                                                    } else {
                                                        text = aktHodInfo;
                                                        if (finalZmenaPredmetu) {
                                                            if (!supl.getPredmet().equals("")) {
                                                                text += "<BR>Změna předmětu: <font color='red'>" + supl.getPredmet() + "</font>";
                                                            } else if (!supl.getPoznamka().equals("")) {
                                                                text += "<BR>Změna: <font color='red'>" + supl.getPoznamka() + "</font>";
                                                            } else {
                                                                text += "<BR>Změna: <font color='red'>" + "Jiné" + "</font>";
                                                            }
                                                        }
                                                        if (finalZastup) {
                                                            text += "<BR>Zastupující: <font color='red'>" + supl.getZastupujici() + "</font>";
                                                        }
                                                        if (finalZmenaUcebny && !supl.getUcebna().equals("")) {
                                                            text += "<BR>Změna učebny: <font color='red'>" + supl.getUcebna() + "</font>";
                                                        }
                                                        if (!supl.getPoznamka().equals("")) {
                                                            text += "<BR>Pozn: <font color='red'>" + supl.getPoznamka() + "</font>";
                                                        }
                                                        if (!supl.getTyp().equals("")) {
                                                            text += "<BR>Typ: <font color='red'>" + supl.getTyp() + "</font>";
                                                        }
                                                    }
                                                    mainActivity.displayToast(Html.fromHtml(text));

                                                    // }
                                                }
                                            });
                                        }
                                    }
                                }

                                if (hoursToAdd.size() > 0) {
                                    for (Supl suplToAdd : hoursToAdd) {
                                        suplMap.get(aktualniDatum).put(mainActivity.trida, suplToAdd);
                                    }
                                    drawTableLayout(rozvrhObj, suplMap, mainActivity);
                                }

                            }
                        }
                    }
                }
                tableRow.addView(textView, textviewParams);
            }
            table.addView(tableRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, 0, 1f));
        }
        mainActivity.setContentView(table);
        Log.d("Pocet", "Zmeny uceben: " + zmenaCounter + ", Suply: " + suplCounter);
        Log.d("Duration", System.currentTimeMillis() - cas + "ms");
        Log.d("NotDefault: ", mainActivity.notDefaultCounter+"");
    }

    public String getAktualniHodInclBreak(String aktualniHod) {
        List<String> hodinyList = Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9"); // HARDCODED
        Map<String, List<LocalTime>> casyMapy = tf.getCasyMap();
        //String aktualniHod = tf.getCurrentHodina(hodinyList, casyMapy);

        //int maxId = hodinyList.size() * 6;
        if (aktualniHod.contains("p")) {

            aktualniHod = Integer.toString(Integer.parseInt((aktualniHod.replace("p", ""))) + 1); //PRASARNA - Napicu zapis...
        }
        return aktualniHod;
    }

    public void getQuizValue(int number){
        double answer;
        if (prevQuizCount == number){
            answer = Math.pow(number, number-1);

            if (number != 9) {
                if (number == 3){
                    displayToast("Nejspíš nuda v hodině, co?\nJo, taky to znám:)");
                } else {
                    prevQuizCount = number;
                    displayToast(decimalFormat.format(answer));
                }
            } else {
                //show dialogbox
                final EditText input = new EditText(mContext);
                new AlertDialog.Builder(mContext)
                        .setTitle("9 = ?")
                        .setView(input)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                String value = input.getText().toString();
                                solveQuiz(value);
                            }
                        }).show();
            }
            prevQuizCount ++;

        } else {
            prevQuizCount = 1;
        }
    }

    private void solveQuiz(String value){
        int hodnota = 0;
        try
        {
            hodnota = Integer.parseInt(value);
        } catch(NumberFormatException nfe)
        {
            displayToast("Smůla :P");
        }
        finally {
            if (hodnota == 43046721){
                displayToast("Geek mode unlocked!");
            } else {
                displayToast("Smůla :P");
            }
        }

    }

    public void displayToast(final String message) {
        myToast.cancel();
        myToast = Toast.makeText(mContext, message, Toast.LENGTH_SHORT);
        myToast.show();
    }
/*
    public void updateTimeTable(Map<LocalDate, Multimap<String, Supl>> suplMap){
        for (Rozvrh rozvrh: rozvrh)
    }*/
}

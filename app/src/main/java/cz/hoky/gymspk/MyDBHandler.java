package cz.hoky.gymspk;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MyDBHandler extends SQLiteOpenHelper {

    private static MyDBHandler sInstance;

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "data2.db";

    // TABULKA ROZVRHY
    public static final String TABLE_ROZVRHY = "rozvrhy";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_TRIDA = "trida";
    public static final String COLUMN_DEN = "den";
    public static final String COLUMN_HODINA = "hodina";
    public static final String COLUMN_PREDMET = "predmet";
    public static final String COLUMN_ZKPREDMET = "zkpredmet";
    public static final String COLUMN_ZKUCITEL = "zkucitel";
    public static final String COLUMN_UCITEL = "ucitel";
    public static final String COLUMN_UCEBNA = "ucebna";
    public static final String COLUMN_ZKUCEBNA = "zkucebna";
    public static final String COLUMN_ISDEFAULT = "isDefault";

    // TABULKA SUPLY
    public static final String TABLE_SUPLY = "suply";

    public static final String COLUMN_SUPL_ID = "_id";
    public static final String COLUMN_SUPL_DEN = "den";
    public static final String COLUMN_SUPL_TRIDA = "trida";
    public static final String COLUMN_SUPL_HODINA = "hodina";
    public static final String COLUMN_SUPL_PREDMET = "predmet";
    public static final String COLUMN_SUPL_VYUCUJICI = "vyucujici";
    public static final String COLUMN_SUPL_ZASTUPUJICI = "zastupujici";
    public static final String COLUMN_SUPL_UCEBNA = "ucebna";
    public static final String COLUMN_SUPL_TYP = "typ";
    public static final String COLUMN_SUPL_POZNAMKA = "poznamka";
    public static final String COLUMN_SUPL_DUVOD = "duvod";

    public static MyDBHandler getInstance(Context context){
        if (sInstance == null){
            sInstance = new MyDBHandler(context.getApplicationContext());
        }
        return sInstance;
    }

    private MyDBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }
    private MyDBHandler(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    private static final String SQL_CREATE_TABLE_ROZVRHY = "CREATE TABLE IF NOT EXISTS " + TABLE_ROZVRHY + "(" +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_TRIDA + " TEXT, " +
            COLUMN_DEN + " TEXT, " +
            COLUMN_HODINA + " TEXT, " +
            COLUMN_PREDMET + " TEXT, " +
            COLUMN_ZKPREDMET + " TEXT, " +
            COLUMN_UCITEL + " TEXT, " +
            COLUMN_ZKUCITEL + " TEXT, " +
            COLUMN_UCEBNA + " TEXT, " +
            COLUMN_ZKUCEBNA + " TEXT, " +
            COLUMN_ISDEFAULT + " INTEGER " +
            ");";

    private static final String SQL_CREATE_TABLE_SUPLY = "CREATE TABLE IF NOT EXISTS " + TABLE_SUPLY + "("
            + COLUMN_SUPL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_SUPL_DEN + " TEXT, "
            + COLUMN_SUPL_TRIDA + " TEXT, "
            + COLUMN_SUPL_HODINA + " TEXT, "
            + COLUMN_SUPL_PREDMET + " TEXT, "
            + COLUMN_SUPL_VYUCUJICI + " TEXT, "
            + COLUMN_SUPL_ZASTUPUJICI + " TEXT, "
            + COLUMN_SUPL_UCEBNA + " TEXT, "
            + COLUMN_SUPL_TYP + " TEXT, "
            + COLUMN_SUPL_POZNAMKA + " TEXT, "
            + COLUMN_SUPL_DUVOD + " TEXT "
            +");";

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE_ROZVRHY);
        db.execSQL(SQL_CREATE_TABLE_SUPLY);
    }

    public void createTables(){
        SQLiteDatabase db = getReadableDatabase();
        db.execSQL(SQL_CREATE_TABLE_ROZVRHY);
        db.execSQL(SQL_CREATE_TABLE_SUPLY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS" + TABLE_ROZVRHY);
        onCreate(db);
    }

    public void setDefault(Hodina hodina, boolean bool){
        SQLiteDatabase db = getReadableDatabase();
        int intFromBool = (bool) ? 1 : 0;

        String strSQL = "UPDATE " +TABLE_ROZVRHY +
                " SET " + COLUMN_ISDEFAULT + " = " + intFromBool +
                " WHERE " + COLUMN_TRIDA + " = '" + hodina.getTrida() +
                "' AND " + COLUMN_DEN + " = '" + hodina.getDen()+
                "' AND " + COLUMN_HODINA + " = '" + hodina.getHod()+
                "' AND " + COLUMN_ZKPREDMET + " = '" + hodina.getZkPredmet()+
                "' AND " + COLUMN_ZKUCITEL + " = '" + hodina.getZkVyucujici() + "'";

        db.execSQL(strSQL);
    }

    public void emptyTable(String tableName){
        SQLiteDatabase db = getWritableDatabase();
        db.delete(tableName, null, null);
    }

    public boolean checkDataBase(Context context){

        SQLiteDatabase checkDB = null;
        try{
            checkDB = SQLiteDatabase.openDatabase(context.getDatabasePath(DATABASE_NAME).toString(), null, SQLiteDatabase.OPEN_READONLY);
            checkDB.close();

        }catch(SQLiteException e){
            Log.d("DB", "Db doesnt exist yet");
        }
        return checkDB != null;
    }

    public boolean tableNotEmpty(String table){

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM " + table, null);

        if(cursor != null){

            cursor.moveToFirst();
            int count = cursor.getInt(0);
            if(count > 0){
                cursor.close();
                return true;
            }
            cursor.close();
        }

        return false;
    }

}

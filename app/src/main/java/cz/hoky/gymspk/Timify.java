package cz.hoky.gymspk;

import org.joda.time.*;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.*;

public class Timify {

    public Map<String, List<LocalTime>> getCasyMap() {

        List<List<String>> casyList = getCasyList();

        List<String> hodinyList = Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9");

        Map<String, List<LocalTime>> timifiedMap = new HashMap<String, List<LocalTime>>();
        for (int i = 0; i< hodinyList.size(); i++){
            LocalTime timifiedStartHour = timifyHour(casyList.get(i).get(0));
            LocalTime timifiedEndHour = timifyHour(casyList.get(i).get(1));
            List<LocalTime> timifiedInterval = Arrays.asList(timifiedStartHour, timifiedEndHour);

            timifiedMap.put(hodinyList.get(i), timifiedInterval);

            if (i+1 != hodinyList.size()){
                timifiedMap.put(hodinyList.get(i)+"p", generateBreakInterval(timifiedEndHour, timifyHour(casyList.get(i+1).get(0))));
            }
        }
        return timifiedMap;
    }

    public List<LocalTime> generateBreakInterval(LocalTime end, LocalTime start){
        return Arrays.asList(end, start);
    }

    public LocalTime timifyHour(String str){
        DateTimeFormatter hodMin = DateTimeFormat.forPattern("HH:mm");

        return hodMin.parseLocalTime(str);
    }

    public boolean isTimeContained (LocalTime now, List<LocalTime> interval){
        if (now.isAfter(interval.get(0)) && now.isBefore(interval.get(1))){
            System.out.println("Time contained!");
            return true;
        }
        return false;
    }

    public List<List<String>> getCasyList(){
        List<List<String>> casyList = new ArrayList<List<String>>();
        ArrayList<String> casInterval = new ArrayList<String>();
        casInterval.add("08:00");
        casInterval.add("08:45");
        casyList.add(casInterval);
        casInterval = new ArrayList<String>();
        casInterval.add("08:55");
        casInterval.add("09:40");
        casyList.add(casInterval);
        casInterval = new ArrayList<String>();
        casInterval.add("10:00");
        casInterval.add("10:45");
        casyList.add(casInterval);
        casInterval = new ArrayList<String>();
        casInterval.add("10:55");
        casInterval.add("11:40");
        casyList.add(casInterval);
        casInterval = new ArrayList<String>();
        casInterval.add("11:50");
        casInterval.add("12:35");
        casyList.add(casInterval);
        casInterval = new ArrayList<String>();
        casInterval.add("12:45");
        casInterval.add("13:30");
        casyList.add(casInterval);
        casInterval = new ArrayList<String>();
        casInterval.add("13:35");
        casInterval.add("14:20");
        casyList.add(casInterval);
        casInterval = new ArrayList<String>();
        casInterval.add("14:25");
        casInterval.add("15:10");
        casyList.add(casInterval);
        casInterval = new ArrayList<String>();
        casInterval.add("15:15");
        casInterval.add("16:00");
        casyList.add(casInterval);

        return casyList;
    }

    public String getCurrentHodina(List<String> hodiny, Map<String, List<LocalTime>> casyMap){
        LocalTime now = new LocalTime();
        for (Map.Entry<String, List<LocalTime>> entry: casyMap.entrySet()){
            if (isTimeContained(now, entry.getValue())){
                return entry.getKey();
            }
        }
        return null;
    }
}

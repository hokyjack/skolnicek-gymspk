package cz.hoky.gymspk;

import org.joda.time.DateTimeConstants;
import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Datify {

    List<String> dnyList = Arrays.asList("Pondělí", "Úterý", "Středa", "Čtvrtek", "Pátek", "Sobota", "Neděle");

    public List<LocalDate> getDatesOfWeek(int weekShift){
        List<LocalDate> dates = new ArrayList<>();

        LocalDate now = new LocalDate();
        if (now.dayOfWeek().get() == DateTimeConstants.SATURDAY || now.dayOfWeek().get() == DateTimeConstants.SUNDAY){
            now = now.plusWeeks(1);
        }

        now = now.plusWeeks(weekShift);

        for (int i = 1; i<6; i++){
            dates.add(now.withDayOfWeek(i));
        }

        return dates;
    }

    public String getDayAsString(int day){
        return dnyList.get(day);
    }
/*
    public LocalDate stringToLocalDate(String rawDatum){

        return datum;
    }*/

}

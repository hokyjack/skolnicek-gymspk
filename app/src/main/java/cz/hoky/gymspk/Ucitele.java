package cz.hoky.gymspk;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

public class Ucitele {

    public BiMap<String, String> getZkratky(){
        BiMap<String, String> zkratky = HashBiMap.create();
        zkratky.put("An", "Andrlová");
        zkratky.put("Br", "Bartoš");
        zkratky.put("Be", "Bezděk");
        zkratky.put("Bh", "Bihuncová");
        zkratky.put("Bn", "Brauner");
        zkratky.put("Bá", "Brožová");
        zkratky.put("Fk", "Flek");
        zkratky.put("Gu", "Guričová");
        zkratky.put("Ha", "Haag");
        zkratky.put("Hj", "Haag J.");
        zkratky.put("Hg", "Hegerová");
        zkratky.put("Hs", "Hlavsa");
        zkratky.put("Hr", "Hradišová");
        zkratky.put("Já", "Jankovská");
        zkratky.put("Jr", "Jílková");
        zkratky.put("Kn", "Kníř");
        zkratky.put("Ká", "Kučerová");
        zkratky.put("Ln", "Lónová");
        zkratky.put("Mc", "Maceček");
        zkratky.put("Má", "Machačová");
        zkratky.put("Int", "Mcintosh");
        zkratky.put("Mk", "Měšťánek");
        zkratky.put("Mš", "Mikuláš");
        zkratky.put("Mi", "Minářová");
        zkratky.put("Mu", "Mutinová");
        zkratky.put("No", "Nováková");
        zkratky.put("Op", "Opekar");
        zkratky.put("Or", "Opekarová");
        zkratky.put("Pv", "Pálková");
        zkratky.put("Pk", "Papoušek");
        zkratky.put("Pš", "Polášek");
        zkratky.put("Po", "Polišenská");
        zkratky.put("Pp", "Pospíšilová");
        zkratky.put("Pr", "Procházková");
        zkratky.put("Pu", "Průša");
        zkratky.put("Rá", "Ryšavá");
        zkratky.put("Ří", "Říhovský");
        zkratky.put("Sb", "Schubertová");
        zkratky.put("St", "Strnad");
        zkratky.put("Sv", "Svatoň");
        zkratky.put("Šr", "Šrámková");
        zkratky.put("Šu", "Šušková");
        zkratky.put("Ts", "Tesařová");
        zkratky.put("Vh", "Vaňková");
        zkratky.put("Vá", "Vavříková");
        zkratky.put("Vi", "Vinklerová");
        zkratky.put("Vo", "Voráč");
        zkratky.put("Vr", "Voráčová");
        zkratky.put("Vd", "Vykydal");
        zkratky.put("Zt", "Zatloukalová");
        zkratky.put("Ze", "Zelená");
        zkratky.put("Žá", "Žáková");

        return zkratky;
    }
}

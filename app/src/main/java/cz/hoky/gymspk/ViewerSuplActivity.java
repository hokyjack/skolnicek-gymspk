package cz.hoky.gymspk;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.collect.BiMap;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

public class ViewerSuplActivity extends Activity
{
    private GridView gridView;
    private WebView browser;
    BiMap<String, String> zkratky;
    List<Supl> suplList;

    public static Toast myToast;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myToast = Toast.makeText(this, "", Toast.LENGTH_SHORT);

        setContentView(R.layout.supl_view);

        Datify dt = new Datify();
        final SuplDAO suplDAO = new SuplDAO(this);

        class MyComparator implements Comparator<LocalDate> {
            public int compare(LocalDate a, LocalDate b) {
                LocalDate aStr, bStr;
                aStr = a;
                bStr = b;
                return bStr.compareTo(aStr);
            }
        }
        TreeSet<LocalDate> dates = new TreeSet<>(new MyComparator());

        TreeSet<LocalDate> unsortedDates = suplDAO.getDates();

        if (unsortedDates.size() > 0) {
            for (LocalDate date : unsortedDates) {
                dates.add(date);
            }

            final List<String> stringDates = new ArrayList<>();
            final Map<String, LocalDate> dateMap = new HashMap<>();
            for (LocalDate date : dates) {
                String stringedDate = date.toString("d. M.") + " - " + dt.getDayAsString(date.dayOfWeek().get() - 1);
                stringDates.add(stringedDate); //toString() or the appropriate method
                dateMap.put(stringedDate, date);
            }
            //TreeMap<LocalDate,dateMap

            Spinner spinner = (Spinner) findViewById(R.id.dates_spinner);
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, stringDates); //selected item will look like a spinner set from XML
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(spinnerArrayAdapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    suplList = suplDAO.getSuplyByDateAsArray(dateMap.get(stringDates.get(position)));
                    drawTable(suplList);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                    // your code here
                }

            });

            Ucitele ut = new Ucitele();
            zkratky = ut.getZkratky();

            if (dates.size() > 0) {
                suplList = suplDAO.getSuplyByDateAsArray(dates.first()); // vybere posledni suplList
                drawTable(suplList);
            }
        }else {
            displayToast("Žádné suply k dispozici\nZkuste stáhnout suply kliknutím na tlačítko obnovit");
        }
    }

    public void displayToast(final String message) {
        myToast.cancel();
        myToast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        myToast.show();
    }

    public void drawTable(List<Supl> suplList){

        TableRow.LayoutParams textviewParams = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 1f);
        int dpValue = 1;
        float d = this.getResources().getDisplayMetrics().density;
        int margin = (int)(dpValue * d); // margin in pixels
        textviewParams.setMargins(margin, 0, 0, 0);

        TableLayout table = (TableLayout)findViewById(R.id.tableLayout);
       // table.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT));
        table.removeAllViews();

        String poznamka = "";

        int id = 1;
        int x = 0;

        TableRow tableRow = new TableRow(this);
        tableRow.setOrientation(TableLayout.HORIZONTAL);

        for (int col = 0; col < 7; col++){

            TextView textView = new TextView(this);
            textView.setHorizontallyScrolling(true);
            textView.setGravity(Gravity.CENTER_HORIZONTAL);
            textView.setId(id++);
            textView.setTag(textView.getId());
            textView.setTypeface(null, Typeface.BOLD);
            textView.setBackgroundColor(Color.LTGRAY);

            switch (col){
                case 0: textView.setText("Třída"); break;
                case 1: textView.setText("Učitel"); break;
                case 2: textView.setText("Hod"); break;
                case 3: textView.setText("Zástup"); break;
                case 4: textView.setText("Předmět"); break;
                case 5: textView.setText("Učebna"); break;
                case 6: textView.setText("Pozn"); break;
            }

            tableRow.addView(textView, textviewParams);
        }
        table.addView(tableRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, 0, 1f));

        for (Supl supl: suplList){

            if (supl.getTyp().equals("POZN")){
                poznamka = supl.getPoznamka();
                continue;
            }

            x++;
            tableRow = new TableRow(this);
            tableRow.setOrientation(TableLayout.HORIZONTAL);

            for (int y = 0; y < 7; y++){

                TextView textView = new TextView(this);
                textView.setHorizontallyScrolling(true);
                textView.setGravity(Gravity.CENTER_HORIZONTAL);
                textView.setId(id++);
                textView.setTag(textView.getId());

                String name = "";

                if (y == 0){ name = supl.getTrida(); }
                else if (y == 1) { name = supl.getVyucujici(); }
                /*else if (y == ++i) {
                    name = supl.getDuvod();

                    if (name.length() > 4){
                        name = name.substring(0,4);
                    }
                }*/
                else if (y == 2) { name = supl.getHodina(); }
                else if (y == 3) {
                    if (zkratky.get(supl.getZastupujici()) != null ){
                        name = zkratky.get(supl.getZastupujici());
                    } else {
                        name = supl.getZastupujici();
                        /*
                        if (name.length() > 2){
                            name = name.substring(0,2);
                        }*/
                    }
                }
                else if (y == 4) { name = supl.getPredmet(); }
                else if (y == 5) { name = supl.getUcebna(); }
                else if (y == 6) {
                    name = supl.getPoznamka();
                    if (supl.getPoznamka().equals("")) {
                        name = supl.getTyp();
                    }
                }


                if ( x%2 == 0){
                    textView.setBackgroundColor(Color.LTGRAY);
                }

                textView.setText(name);
                tableRow.addView(textView, textviewParams);
            }
            table.addView(tableRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, 0, 1f));
        }
        if (!poznamka.equals("")){

            TextView textView = (TextView) findViewById(R.id.poznamkaView);
            //textView.setHorizontallyScrolling(true);
            textView.setGravity(Gravity.START);
            textView.setTag(textView.getId());
            textView.setText(Html.fromHtml(poznamka));
        }
    }
}
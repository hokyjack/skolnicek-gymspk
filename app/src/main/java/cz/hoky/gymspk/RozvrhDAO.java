package cz.hoky.gymspk;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RozvrhDAO {

    public static final String TAG = "RozvrhDAO";

    // Database fields
    private SQLiteDatabase db;
    private MyDBHandler myDBHandler;
    private Context mContext;

    public RozvrhDAO(Context context) {
        myDBHandler = MyDBHandler.getInstance(context);

        this.mContext = context;
        //open database
        try {
            open();
        } catch (SQLException e) {
            Log.e("RozvrhDAO", "Cant open DB!");
           // Log.e(TAG, "SQLException on openning database " + e.getMessage());
        }
    }
    public void open() throws SQLException{
        db = myDBHandler.getWritableDatabase();
    }

    public void close(){
        myDBHandler.close();
    }

    public void saveAllRozvrhyToDb(List<Rozvrh> rozvrhy) {

        long time = System.currentTimeMillis();

        String sql = "INSERT INTO " + MyDBHandler.TABLE_ROZVRHY + " ("
                + MyDBHandler.COLUMN_TRIDA + ", "
                + MyDBHandler.COLUMN_DEN + ", "
                + MyDBHandler.COLUMN_HODINA + ", "
                + MyDBHandler.COLUMN_ZKPREDMET + ", "
                + MyDBHandler.COLUMN_ZKUCITEL + ", "
                + MyDBHandler.COLUMN_ZKUCEBNA + ", "
                + MyDBHandler.COLUMN_PREDMET + ", "
                + MyDBHandler.COLUMN_UCITEL + ", "
                + MyDBHandler.COLUMN_UCEBNA + ", "
                + MyDBHandler.COLUMN_ISDEFAULT + ") VALUES (?,?,?,?,?,?,?,?,?,?);";

        SQLiteStatement stmt = db.compileStatement(sql);
        db.beginTransaction();
        try {
            for (Rozvrh rozvrh : rozvrhy) {
                for (Hodina hodina : rozvrh.getAllHodinyAsList()) {
                    stmt.clearBindings();
                    stmt.bindString(1, hodina.getTrida());
                    stmt.bindString(2, hodina.getDen());
                    stmt.bindString(3, hodina.getHod());
                    stmt.bindString(4, hodina.getZkPredmet());
                    stmt.bindString(5, hodina.getZkVyucujici());
                    stmt.bindString(6, hodina.getZkUcebna());
                    stmt.bindString(7, hodina.getPredmet());
                    stmt.bindString(8, hodina.getVyucujici());
                    stmt.bindString(9, hodina.getUcebna());
                    stmt.bindLong(10, hodina.getDefaultInt());
                    stmt.execute();
                }
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            //fucked up - nepovedlo se rozvrhy ulozit do databaze ( ukazat toast nebo dialog)
            Log.d("Shit!", "No data saved");

        } finally{
            db.endTransaction();
        }

        Log.d("Transaction complete", System.currentTimeMillis()-time+"ms");
    }

    public List<Rozvrh> getAllRozvrhy(){
        List<Rozvrh> rozvrhy = new ArrayList<>();
        Map<String, Multimap<String, Hodina>> rozvrh = new HashMap<>();
        List<String> dnyList = Arrays.asList("pondělí", "úterý", "středa", "čtvrtek", "pátek");
        List<String> hodinyList = Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9");

        //List<List<String>> casyList = tf.getCasyList();
        //Map<String, List<LocalTime>> casyMap = tf.getCasyMap(); // HARDCODED !

        String trida = "";
        String den, hod, predmet, ucitel, ucebna, zkPredmet, zkUcitel, zkUcebna;
        boolean isDefault;

        //SQLiteDatabase db = getReadableDatabase(); //-commented
        String query = "SELECT * FROM " + MyDBHandler.TABLE_ROZVRHY + " WHERE 1 ORDER BY "
                + MyDBHandler.COLUMN_TRIDA + ", " + MyDBHandler.COLUMN_HODINA + ", " + MyDBHandler.COLUMN_ZKPREDMET + ", " + MyDBHandler.COLUMN_ZKUCITEL;

        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();

        // nazev tridy prvni v databazi
        String tr = c.getString(c.getColumnIndex(MyDBHandler.COLUMN_TRIDA));

        int pocetHodin = 0;
        while(!c.isAfterLast()){

            boolean zmenaTridy = false;

            // if dalsi trida -> zmenaTridy = true
            if (!c.getString(c.getColumnIndex(MyDBHandler.COLUMN_TRIDA)).equals(tr)) {
                zmenaTridy = true;
            }

            if (zmenaTridy){
                Rozvrh rozvrhObj = new Rozvrh(tr, rozvrh, dnyList, hodinyList, pocetHodin);
                //Log.d("DB: ", rozvrhObj.getTrida());
                //Log.d("DB: ", rozvrhObj.getRozvrh().toString());

                rozvrhy.add(rozvrhObj);

                tr = c.getString(c.getColumnIndex(MyDBHandler.COLUMN_TRIDA));
                rozvrh = new HashMap<>();

            }

            trida = c.getString(c.getColumnIndex(MyDBHandler.COLUMN_TRIDA));
            den = c.getString(c.getColumnIndex(MyDBHandler.COLUMN_DEN));
            hod = c.getString(c.getColumnIndex(MyDBHandler.COLUMN_HODINA));
            predmet = c.getString(c.getColumnIndex(MyDBHandler.COLUMN_PREDMET));
            zkPredmet = c.getString(c.getColumnIndex(MyDBHandler.COLUMN_ZKPREDMET));
            ucitel = c.getString(c.getColumnIndex(MyDBHandler.COLUMN_UCITEL));
            zkUcitel = c.getString(c.getColumnIndex(MyDBHandler.COLUMN_ZKUCITEL));
            ucebna = c.getString(c.getColumnIndex(MyDBHandler.COLUMN_UCEBNA));
            zkUcebna = c.getString(c.getColumnIndex(MyDBHandler.COLUMN_ZKUCEBNA));
            isDefault = (c.getInt(c.getColumnIndex(MyDBHandler.COLUMN_ISDEFAULT)) == 1);

            Hodina hodina = new Hodina(trida, predmet, zkPredmet, ucitel, zkUcitel, ucebna, zkUcebna, den, hod, isDefault);
            pocetHodin++;

            Multimap<String, Hodina> multimap = rozvrh.get(hodina.den);
            if (multimap == null) {
                multimap = HashMultimap.create();
                rozvrh.put(hodina.den, multimap);
            }
            multimap.put(hodina.hod, hodina);

            if (c.isLast()){
                Rozvrh rozvrhObj = new Rozvrh(tr, rozvrh, dnyList, hodinyList);
                rozvrhy.add(rozvrhObj);

                tr = c.getString(c.getColumnIndex(MyDBHandler.COLUMN_TRIDA));
                rozvrh = new HashMap<>();
            }
            c.moveToNext();
        }

        c.close();
        return rozvrhy;
    }

}

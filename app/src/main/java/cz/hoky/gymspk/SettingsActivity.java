package cz.hoky.gymspk;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;

import java.util.ArrayList;

public class SettingsActivity extends PreferenceActivity {

    MyDBHandler dbHandler;
    private Context mContext = null;

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        mContext = this;
        dbHandler = MyDBHandler.getInstance(this);

        isDeleteAvaible();

        ListPreference listPreferenceCategory = (ListPreference) findPreference("vychozi_trida");
        if (listPreferenceCategory != null) {

            ArrayList<String> tridyList = (ArrayList<String>) getIntent().getSerializableExtra("tridyList");

            CharSequence entries[] = new String[tridyList.size()];

            int i = 0;
            for (String trida : tridyList) {
                entries[i] = trida;
                i++;
            }
            listPreferenceCategory.setEntries(entries);
            listPreferenceCategory.setEntryValues(entries);
        }
/*
        getPreferenceScreen().findPreference("pref_key_supl_frequency").setEnabled(false);
        getPreferenceScreen().findPreference("pref_key_use_data").setEnabled(false);
        getPreferenceScreen().findPreference("pref_key_zvyraznit_nedefault").setEnabled(false);
        getPreferenceScreen().findPreference("pref_key_zvyraznit_multiple").setEnabled(false);

        getPreferenceScreen().findPreference("pref_key_supl_frequency").setEnabled(false);
        getPreferenceScreen().findPreference("pref_key_supl_sync").setEnabled(false);*/

        Preference smazatSuply = findPreference("pref_smazat_suply");
        smazatSuply.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Opravdu chcete smazat uložené suply?");
                builder.setMessage("Změny se projeví po restartu aplikace");
                builder.setNegativeButton("Ne",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                builder.setPositiveButton("Ano",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dbHandler.emptyTable(MyDBHandler.TABLE_SUPLY);
                                isDeleteAvaible();

                            }
                        });
                builder.create();
                builder.show();

                return true;
            }
        });
        Preference smazatRozvrhy = findPreference("pref_smazat_rozvrhy");
        smazatRozvrhy.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Opravdu chcete smazat uložené rozvrhy?");
                builder.setMessage("Změny se projeví po restartu aplikace");
                builder.setNegativeButton("Ne",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                builder.setPositiveButton("Ano",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dbHandler.emptyTable(MyDBHandler.TABLE_ROZVRHY);
                                isDeleteAvaible();
                            }
                        });
                builder.create();
                builder.show();

                return true;
            }
        });

    }

    private void isDeleteAvaible() {
        if (dbHandler.tableNotEmpty(MyDBHandler.TABLE_ROZVRHY)){
            getPreferenceScreen().findPreference("pref_smazat_rozvrhy").setEnabled(true);
        } else {
            getPreferenceScreen().findPreference("pref_smazat_rozvrhy").setEnabled(false);
        }
        if (dbHandler.tableNotEmpty(MyDBHandler.TABLE_SUPLY)){
            getPreferenceScreen().findPreference("pref_smazat_suply").setEnabled(true);
        } else {
            getPreferenceScreen().findPreference("pref_smazat_suply").setEnabled(false);
        }
    }
}

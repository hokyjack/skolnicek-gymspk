package cz.hoky.gymspk;

import android.util.Log;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Hodina{

    private  int _id;
    String trida;
    String predmet;
    String zkPredmet;
    String vyucujici;
    String zkVyucujici;
    String ucebna;
    String zkUcebna;
    String den;
    String hod;
    Boolean isDefault;

    public class ChainedComparator<T> implements Comparator<T> {
        private List<Comparator<T>> simpleComparators;
        @SafeVarargs
        public ChainedComparator(Comparator<T>... simpleComparators) {
            this.simpleComparators = Arrays.asList(simpleComparators);
        }
        public int compare(T o1, T o2) {
            for (Comparator<T> comparator : simpleComparators) {
                int result = comparator.compare(o1, o2);
                if (result != 0) {
                    return result;
                }
            }
            return 0;
        }
    }

    public static Comparator<Hodina> COMPARE_BY_ZKPREDMET = new Comparator<Hodina>() {
        public int compare(Hodina one, Hodina other) {
            if (one.zkPredmet.compareTo(other.zkPredmet) == 0){

            }
            return one.zkPredmet.compareTo(other.zkPredmet);
        }
    };

    enum HodinaComparator implements Comparator<Hodina> {
        ZKPREDMET_SORT {
            public int compare(Hodina o1, Hodina o2) {
                return o1.getZkPredmet().compareTo(o2.getZkPredmet());
            }},
        ZKUCITEL_SORT {
            public int compare(Hodina o1, Hodina o2) {
                return o1.getZkVyucujici().compareTo(o2.getZkVyucujici());
            }};

        public static Comparator<Hodina> decending(final Comparator<Hodina> other) {
            return new Comparator<Hodina>() {
                public int compare(Hodina o1, Hodina o2) {
                    return -1 * other.compare(o1, o2);
                }
            };
        }

        public static Comparator<Hodina> getComparator(final HodinaComparator... multipleOptions) {
            return new Comparator<Hodina>() {
                public int compare(Hodina o1, Hodina o2) {
                    for (HodinaComparator option : multipleOptions) {
                        int result = option.compare(o1, o2);
                        if (result != 0) {
                            return result;
                        }
                    }
                    return 0;
                }
            };
        }
    }

    public Hodina() {
    }

    public Hodina(Hodina newHodaina){
        this.trida = newHodaina.trida;
        this.predmet = newHodaina.predmet;
        this.zkPredmet = newHodaina.zkPredmet;
        this.vyucujici = newHodaina.vyucujici;
        this.zkVyucujici = newHodaina.zkVyucujici;
        this.ucebna = newHodaina.ucebna;
        this.zkUcebna = newHodaina.zkUcebna;
        this.den = newHodaina.den;
        this.hod = newHodaina.hod;
        this.isDefault = newHodaina.isDefault;
    }

    public Hodina(String trida, String predmet, String zkPredmet, String vyucujici, String zkVyucujici, String ucebna, String zkUcebna, String den, String hod, boolean isDefault) {
        this.trida = trida;
        this.predmet = predmet;
        this.zkPredmet = zkPredmet;
        this.vyucujici = vyucujici;
        this.zkVyucujici = zkVyucujici;
        this.ucebna = ucebna;
        this.zkUcebna = zkUcebna;
        this.den = den;
        this.hod = hod;
        this.isDefault = isDefault;
    }

    public String getTrida() {
        return trida;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public int getDefaultInt() {
        if (isDefault){
            return 1;
        } else {
            return 0;
        }
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public void setTrida(String trida) {
        this.trida = trida;
    }

    public void setPredmet(String predmet){
        this.predmet = predmet;
    }

    public void setZkPredmet (String zkratka){
        this.zkPredmet = zkratka;
    }

    public void setVyucujici (String vyucujici){
        this.vyucujici = vyucujici;
    }

    public void setZkVyucujici(String zkVyucujici) {
        this.zkVyucujici = zkVyucujici;
    }

    public void setUcebna (String ucebna){
        this.ucebna = ucebna;
    }

    public void setZkUcebna(String zkUcebna) {
        this.zkUcebna = zkUcebna;
    }

    public void setDen(String den){
        this.den = den;
    }
    public void setHod (String hod){
        this.hod = hod;
    }

    public String getPredmet() {
        return predmet;
    }

    public String getZkPredmet() {
        return zkPredmet;
    }

    public String getVyucujici() {
        return vyucujici;
    }

    public String getZkVyucujici() {
        return zkVyucujici;
    }

    public String getUcebna() {
        return ucebna;
    }

    public String getZkUcebna() {
        return zkUcebna;
    }

    public String getDen() {
        return den;
    }

    public String getHod() {
        return hod;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public void printHodina(){
        Log.d("Predmet:", "Tahle hodina je: " + zkPredmet + " a uci to: " + zkVyucujici + "ve tride: " + zkUcebna);
    }


}

package cz.hoky.gymspk;

import com.google.common.collect.Multimap;

import org.joda.time.LocalTime;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Rozvrh {
    String trida;
    Map<String, Multimap<String, Hodina>> rozvrh;
    List<String> dny;
    List<String> hodiny;
    int pocetHodin = 0;

    Timify tf = new Timify();
    Map<String, List<LocalTime>> casyMap = tf.getCasyMap();

    public Rozvrh(String trida, Map<String, Multimap<String, Hodina>> rozvrh, List<String> dny, List<String> hodiny, Map<String, List<LocalTime>> casyMap, int pocetHodin) {
        this.trida = trida;
        this.rozvrh = rozvrh;
        this.dny = dny;
        this.hodiny = hodiny;
        this.casyMap = casyMap;
        this.pocetHodin = pocetHodin;
    }

    public Rozvrh(String trida, Map<String, Multimap<String, Hodina>> rozvrh, List<String> dny, List<String> hodiny, int pocetHodin) {
        this.trida = trida;
        this.rozvrh = rozvrh;
        this.dny = dny;
        this.hodiny = hodiny;
        this.pocetHodin = pocetHodin;
    }

    public Rozvrh(String trida, Map<String, Multimap<String, Hodina>> rozvrh, List<String> dny, List<String> hodiny) {
        this.trida = trida;
        this.rozvrh = rozvrh;
        this.dny = dny;
        this.hodiny = hodiny;
    }

    public List<Hodina> getAllHodinyAsList(){
        List<Hodina> hodiny = new ArrayList<>();
        for (Multimap<String, Hodina> value: rozvrh.values()){

            for (Hodina hodina : value.values()) {
                hodiny.add(hodina);
            }
        }
        return hodiny;
    }

    public int getPocetHodin() {
        return pocetHodin;
    }

    public void setPocetHodin(int pocetHodin) {
        this.pocetHodin = pocetHodin;
    }

    public void setCasyMap(Map<String, List<LocalTime>> casyMap){
        this.casyMap = casyMap;
    }

    public Map<String, List<LocalTime>> getCasyMap(){
        return casyMap;
    }

    public String getTrida() {
        return trida;
    }

    public Map<String, Multimap<String, Hodina>> getRozvrh() {
        return rozvrh;
    }

    public List<String> getDny() {
        return dny;
    }

    public List<String> getHodiny() {
        return hodiny;
    }
}
